\chapter{Linear waves and instabilities}

\section{Interfacial waves and instabilities}
This chapter presents a unified treatment of gravity-capillary wave, Rayleigh-Taylor instability and Kelvin-Helmholtz instability.

Consider an infinitely deep pool of static water below an infinite atmosphere of air, with the wind blowing at speed $U$. 
(Here water and air should be considered merely as labels for the two fluids; the analysis applies equally well to any two immiscible fluids.)
Gravity $g$ acts along the negative $y$-axis of the two-dimensional Cartesian coordinate system, pointing towards water.
The interface, which is taken along the $x$-axis, is perpendicular to gravity and has surface tension $\surft$. 
While the interface is initially flat, at $t=0$ the action of an impulsive conservative force perturbs the interface shape to the curve given by $y=h(x,t)$ and sets up a flow in the two fluids.
This analysis is about the subsequent development of the interface shape and the accompanying flow.
Figure~\ref{fig:WavesSchematic} shows the configuration schematically.
Water has density $\rho_w$ and the air has density $\rho_a$.
Both fluids are assumed to be inviscid and incompressible.

\begin{figure}
\centerline{\includegraphics[width=120mm]{WavesInstabilitiesSchematic}}
\caption{Schematic showing two infinitely deep layers of air and water with an interface.}
\label{fig:WavesSchematic}
\end{figure}

\subsection{The governing equations}
The equations governing the flow and interface shape are as follows.
Because both the static water and uniformly flowing air initially lack vorticity, and viscosity and non-conservative forces needed for generating vorticity in the flow are absent, the flow remains irrotational in both fluids.
\begin{enumerate}
 \item {\bf Air:}  The fluid velocity $\bu_a$, the velocity potential $\phi_a$ and the fluid pressure $p_a$ satisfy
 \begin{subequations}
 \label{eqn:wavesa}
 \begin{align}
  \bu_a = \grad \phi_a, \quad \text{and} \quad \grad\cdot\bu_a = 0 \quad \rightarrow \quad \nabla^2 \phi_a = 0, \label{eqn:wavesmassa} \\
  \rho_a \left(\dfrac{\partial \phi_a}{\partial t} + \dfrac{1}{2} |\grad\phi_a|^2 \right) + p_a + \rho_a g y = c = \text{a constant}, \label{eqn:wavesmoma}
 \end{align}
 \end{subequations}
where \eqref{eqn:wavesmassa} represents conservation of mass and \eqref{eqn:wavesmoma} conservation of meomentum in the form of Bernoulli equation for unsteady potential flow, as described in \eqref{eqn:Bernoulliunsteady} from \S\ref{sec:unsteadybernoulli}.
\item {\bf Water:} Denoting the water velocity, velocity potential and pressure by $\bu_w$, $\phi_w$ and $p_w$, the equations governing its flow analogous to the ones in the air are
\begin{subequations}
 \label{eqn:wavesw}
\begin{align}
   \bu_w = \grad \phi_w, \quad \text{and} \quad \grad\cdot\bu_w = 0 \quad \rightarrow \quad \nabla^2 \phi_w = 0, \label{eqn:wavesmassw} \\
  \rho_w \left(\dfrac{\partial \phi_w}{\partial t} + \dfrac{1}{2} |\grad\phi_w|^2 \right) + p_w + \rho_w g y = d = \text{another constant}. \label{eqn:wavesmomw}
\end{align}
\end{subequations}
\item {\bf Dynamic boundary condition:} At the interface, the force per unit area exerted from the two fluids must add up to the unbalanced force due to Laplace pressure \signpost{Chapter on surface tension} on the interface. Mathematically, this amounts to the boundary condition at $y=h$ as
\begin{align}
 p_a = p_w + \surft \kappa, \quad \text{where} \quad \kappa = \left(\dfrac{{\partial^2 h}/{\partial x^2}}{1 + \left( {\partial h}/{\partial x}\right)^{3/2}}\right) = \text{interface mean curvature}. \label{eqn:waveslaplace}
\end{align}
This condition imposes momentum balance over the interface and ensures that forces are transmitted appropriately across it.
\item {\bf Kinematic boundary condition: } The interface moves with the fluid(s) or, in other words, the interface is a ``material property''.  The corresponding mathematical statement is
\begin{align*}
 \dfrac{D}{Dt} \left(h(x,t)-y\right) = 0.
\end{align*}
When the material derivative is expanded and the velocity of the two fluids is substituted, the kinematic boundary condition becomes
\begin{subequations}
\label{eqn:waveskbc}
\begin{align}
 &\pd{h}{t} + \pd{\phi_a}{x} \pd{h}{x} = \pd{\phi_a}{y} \quad \text{at} \quad y=h, \quad \text{and} \label{eqn:waveskbca} \\
 &\pd{h}{t} + \pd{\phi_w}{x} \pd{h}{x} = \pd{\phi_w}{y} \quad \text{at} \quad y=h, \label{eqn:waveskbcw}
\end{align}
\end{subequations}
where \eqref{eqn:waveskbca} uses the air velocity and \eqref{eqn:waveskbcw} uses the water velocity.
\end{enumerate}

Equations (\ref{eqn:wavesa}-\ref{eqn:waveskbc}) constitute the governing equations for the air-water-interface dynamics.

\subsection{Steady state and linearization}
A trivial steady state satisfies of the governing equations:
\begin{subequations}
\begin{align}
 &\bu_a = U\be_x,& \quad &\phi_a = Ux,& \quad &p_a = c-\rho_a g y -\dfrac{1}{2} \rho_a U^2&, \\
 &\bu_w = \boldsymbol{0},& \quad &\phi_w = 0,& \quad &p_w = d-\rho_w g y,& \\
 &h(x,t) = 0.&
\end{align}
\end{subequations}
This flow corresponds to a uniform flow in the air, no flow in the water and a flat interface. 
The configuration is perturbed slightly as
\begin{subequations}
\label{eqn:wavesperturb}
\begin{align}
 &\bu_a = U\be_x + \eps \bu'_a,& \quad &\phi_a = Ux + \eps \phi'_a,& \quad &p_a = \eps p'_a + c-\rho_a g y -\dfrac{1}{2} \rho_a U^2&, \\
 &\bu_w = \boldsymbol{0} + \eps \bu'_w,& \quad &\phi_w = 0 + \phi'_w,& \quad &p_w = \eps p'_w + d-\rho_w g y,& \\
 &h(x,t) = 0 + \eps h'(x,t).&
\end{align}
\end{subequations}
Here $\eps$ parameterizes the size of the perturbation, and the functions decorated with a prime denote a perturbation to the unprimed version of the function.
This transformation of the dependent variables adds little to the analysis by itself, except when the size of the perturbation is assumed to be small.
Mathematically, this assumption implies a vanishingly small $\eps$ and leads to the process of linearization as follows.

Substituting \eqref{eqn:wavesperturb} in the governing equations (\ref{eqn:wavesa}-\ref{eqn:waveskbc}) and canceling a factor of $\eps$, and dropping the prime decoration leads to
\begin{subequations}
\label{eqn:waveslina}
 \begin{align}
    \bu_a = \grad \phi_a, \quad \text{and} \quad \grad\cdot\bu_a = 0 \quad \rightarrow \quad \nabla^2 \phi_a = 0, \label{eqn:waveslinmassa} \\
  \rho_a \left(\dfrac{\partial \phi_a}{\partial t} + U\pd{\phi_a}{x} + \dfrac{\eps}{2} |\grad\phi_a|^2 \right) + p_a = 0 \quad \text{at} \quad y=\eps h, \label{eqn:waveslinmoma}
 \end{align}
\end{subequations}
from \eqref{eqn:wavesa},
\begin{subequations}
 \label{eqn:waveslinw}
\begin{align}
   \bu_w = \grad \phi_w, \quad \text{and} \quad \grad\cdot\bu_w = 0 \quad \rightarrow \quad \nabla^2 \phi_w = 0, \label{eqn:waveslinmassw} \\
  \rho_w \left(\dfrac{\partial \phi_w}{\partial t} + \dfrac{\eps}{2} |\grad\phi_w|^2 \right) + p_w + \rho_w g h = 0 \quad \text{at} \quad y=\eps h, \label{eqn:waveslinmomw}
\end{align}
\end{subequations}
from \eqref{eqn:wavesw},
\begin{align}
 p_a -\rho_a g h = p_w - \rho_w g h + \surft \dfrac{ \partial^2 h/\partial x^2 }{\left( 1 + \eps^2 ({\partial h}/{\partial x})^2\right)^{3/2}} \quad \text{at} \quad y=\eps h, \label{eqn:waveslinlaplace}
\end{align}
from \eqref{eqn:waveslaplace}, and
\begin{subequations}
\label{eqn:waveslinkbc}
\begin{align}
 \pd{h}{t} + U \pd{h}{x} + \eps \pd{\phi_a}{x} \pd{h}{x} &= \pd{\phi_a}{y} \quad \text{at} \quad y=\eps h, \quad \text{and} \label{eqn:waveslinkbca} \\
 \pd{h}{t} + \eps \pd{\phi_w}{x} \pd{h}{x} &= \pd{\phi_w}{y} \quad \text{at} \quad y=\eps h, \label{eqn:waveslinkbcw}
\end{align}
\end{subequations}
from \eqref{eqn:waveskbc}.
Equations (\ref{eqn:waveslina}-\ref{eqn:waveslinkbc}) are in every way equivalent to (\ref{eqn:wavesa}-\ref{eqn:waveskbc}) so far.
Now we apply the defining step of linearization, the limit os vanishingly small $\eps$.
Substituting this limit into (\ref{eqn:waveslina}-\ref{eqn:waveslinkbc}) yields
\begin{subequations}
\label{eqn:wavesllina}
 \begin{align}
    \bu_a = \grad \phi_a, \quad \text{and} \quad \grad\cdot\bu_a = 0 \quad \rightarrow \quad \nabla^2 \phi_a = 0, \label{eqn:wavesllinmassa} \\
  \rho_a \left(\dfrac{\partial \phi_a}{\partial t} + U\pd{\phi_a}{x} \right) + p_a  = 0 \quad \text{at} \quad y=0, \label{eqn:wavesllinmoma}
 \end{align}
\end{subequations}
from \eqref{eqn:waveslina},
\begin{subequations}
 \label{eqn:wavesllinw}
\begin{align}
   \bu_w = \grad \phi_w, \quad \text{and} \quad \grad\cdot\bu_w = 0 \quad \rightarrow \quad \nabla^2 \phi_w = 0, \label{eqn:wavesllinmassw} \\
  \rho_w \left(\dfrac{\partial \phi_w}{\partial t} \right) + p_w = 0 \quad \text{at} \quad y=0, \label{eqn:wavesllinmomw}
\end{align}
\end{subequations}
from \eqref{eqn:waveslinw},
\begin{align}
 p_a -\rho_a g h= p_w - \rho_w g h + \surft \pdd{h}{x} \quad \text{at} \quad y=0, \label{eqn:wavesllinlaplace}
\end{align}
from \eqref{eqn:waveslinlaplace}, and
\begin{subequations}
\label{eqn:wavesllinkbc}
\begin{align}
 \pd{h}{t} + U \pd{h}{x}  &= \pd{\phi_a}{y} \quad \text{at} \quad y=0 \quad \text{and} \label{eqn:wavesllinkbca} \\
 \pd{h}{t} &= \pd{\phi_w}{y} \quad \text{at} \quad y=0, \label{eqn:wavesllinkbcw}
\end{align}
\end{subequations}
from \eqref{eqn:waveslinkbc}.
A defining feature of the resulting (\ref{eqn:wavesllina}-\ref{eqn:wavesllinkbc}) is that they are linear in the dependent variables.
Hence this process of making infinitesimally small amplitude perturbations is called linearization.
Also note, how the boundary conditions are now imposed at $y=0$ instead of the exact interface location $y=\eps h$.

\subsection{Fourier transform}
The most convenient way to interpret the linear equations (\ref{eqn:wavesllina}-\ref{eqn:wavesllinkbc}) is to identify the directions along which translational invariance applies and perform a Fourier transform along them.
In this case, the physical system and the corresponding mathematical model given by (\ref{eqn:wavesllina}-\ref{eqn:wavesllinkbc}) is translationally invariant along $x$ (but not $y$ because of the presence of the interface). 
A shift of the origin along $x$ does not change the equations, but a shift along $y$ does through a change in the nominal position of the interface.
Thus, we will perform a Fourier transform of (\ref{eqn:wavesllina}-\ref{eqn:wavesllinkbc}) along the $x$ axis. 

For this purpose, let us define the transformed variable $\hat{\phi}$ decorated by a caret for the untransformed undecorated variable $\phi$
\begin{subequations}
\label{eqn:Fourier}
\begin{align}
 \hat\phi(k,y,t) &= \dfrac{1}{2\pi} \int_{-\infty}^\infty \phi(x,y,t)e^{-ikx}~\dd x, \quad \text{and} \label{eqn:Fourierfwd} \\
 \phi(x,y,t) &= \int_{-\infty}^\infty \hat{\phi}(k, y, t) e^{ikx}~\dd k. \label{eqn:Fourierbwd}
\end{align}
\end{subequations}
Here \eqref{eqn:Fourierfwd} is the forward Fourier transform and \eqref{eqn:Fourierbwd} is the inverse transform. 
The transform is a function of the variable $k$, which is called the wave number.
And interpretation of \eqref{eqn:Fourierbwd} is that the function in the physical space $\phi(x,y,t)$ is being written as a linear combination of the sinusoids, i.e. $e^{ikx}$, of wavenumber $k$ (equivalently wavelength $2\pi/k$) with amplitude $\hat\phi(k,y,t)$. 
Thus the forward transform \eqref{eqn:Fourierfwd} decomposes a given function into its components and deduces the amplitude of the sinusoid with wavelength $2\pi/k$. 
The amplitudes are in general complex, which accounts for the sine and the cosine part of the sinusoid.
However, because the variable $\phi(x,y,t)$ in the physical space is real, the Fourier transformed amplitude $\hat\phi(k,y,t)$ satisfies $\hat\phi(-k,y,t) = {\hat\phi}^*(k,y,t)$, where the asterisk denotes complex conjugation.

A property of the Fourier transform used in this analysis is the relation between the Fourier transform of functions and its derivatives. 
It may be readily verified using integration by parts that
\begin{align}
  ik \hat\phi(k,y,t) &= \dfrac{1}{2\pi} \int_{-\infty}^\infty \pd{\phi(x,y,t)}{x} e^{-ikx}~\dd x.
\end{align}
Loosely speaking, differentiation with respect to $x$ in the real space is converted to a factor of $ik$ is Fourier transformed space. 

Fourier transform of (\ref{eqn:wavesllina}-\ref{eqn:wavesllinkbc}) and dropping the caret decoration yields
\begin{subequations}
\label{eqn:wavesflina}
 \begin{align}
    &u_a = ik\phi_a, \quad v_a = \pd{\phi_a}{y}, \quad \text{and} \quad iku_a + \pd{v_a}{y} = 0 \quad \rightarrow \quad \left(\pdd{}{y} - k^2 \right) \phi_a = 0, \label{eqn:wavesflinmassa} \\
  &\rho_a \left(\dfrac{\partial \phi_a}{\partial t} + ik U \phi_a \right) + p_a = 0 \quad \text{at} \quad y=0, \label{eqn:wavesflinmoma}
 \end{align}
\end{subequations}
from \eqref{eqn:wavesllina},
\begin{subequations}
\label{eqn:wavesflinw}
 \begin{align}
    &u_w = ik\phi_w, \quad v_w = \pd{\phi_w}{y}, \quad \text{and} \quad iku_w + \pd{v_w}{y} = 0 \quad \rightarrow \quad \left(\pdd{}{y} - k^2 \right) \phi_w = 0, \label{eqn:wavesflinmassw} \\
  &\rho_w \dfrac{\partial \phi_w}{\partial t} + p_w = 0 \quad \text{at} \quad y=0, \label{eqn:wavesflinmomw}
 \end{align}
\end{subequations}
from \eqref{eqn:wavesllinw},
\begin{align}
 p_a - \rho_a g h= p_w - \rho_w h g - \surft k^2 h \quad \text{at} \quad y=0, \label{eqn:wavesflinlaplace}
\end{align}
from \eqref{eqn:wavesllinlaplace}, and
\begin{subequations}
\label{eqn:wavesflinkbc}
\begin{align}
 \pd{h}{t} + ik U h  &= \pd{\phi_a}{y} \quad \text{at} \quad y=0 \quad \text{and} \label{eqn:wavesflinkbca} \\
 \pd{h}{t} &= \pd{\phi_w}{y} \quad \text{at} \quad y=0, \label{eqn:wavesflinkbcw}
\end{align}
\end{subequations}
from \eqref{eqn:wavesllinkbc}.
Equations (\ref{eqn:wavesflina}-\ref{eqn:wavesflinkbc}) are the Fourier transformed versions governing the evolution of the perturbation.
Notably, the amplitudes $\phi_a$, $\phi_w$, $p_a$, $p_w$ and $h$ with wavenumber $k$ evolve independently of other wavenumbers, as inferred from the fact that (\ref{eqn:wavesflina}-\ref{eqn:wavesflinkbc}) do not couple the wavenumber $k$ to any other wavenumber. 
This demonstrates that a sinusoidal perturbation with wavenumber $k$ evolves in time independently of ever other wavenumber that may comprise the general perturbation.
Because the most general perturbation can always be decomposed into its constituent sinusoids using the Fourier transform, each wavenumber then evolved in time independently, and later recomposed using the inverse Fourier transform.
Thus, it suffices to study the evolution of each wavenumber separately.

(Sinusoids are normal modes -- i.e. shape functions which decouple a linear operator -- for translationally invariant systems. If the system of interest is not translationally invariant, the normal modes for that system may be deduced by solving for the eigenfunctions of the underlying linear operator. Because the process of investigating the evolution of small perturbation leads to linear equations, determining normal modes of the underlying linear operators are a common operation in the analysis of waves and instabilities.)

\subsection{Solution to Laplace equation}
The solution to \eqref{eqn:wavesflinmassa} for $\phi_a$ and \eqref{eqn:wavesflinmassw} for $\phi_w$ is 
\begin{subequations}
\label{eqn:wavessollaplace}
 \begin{align}
  \phi_a &= A e^{-|k| y} + B e^{ |k| y} \quad \text{for $y>0$ and} \\
  \phi_w &= C e^{ |k| y} + D e^{-|k| y} \quad \text{for $y<0$},
 \end{align}
\end{subequations}
where $A$, $B$, $C$ and $D$ are constants of integration, themselves functions of $k$ and $t$.
Now we apply the conditions of stagnancy deep into the water layer, and uniform flow high up into the air layer.
This implies that the perturbation to velocity perturbations decay as $|y| \to \infty$.
This is achieved by setting the constants $B=D=0$.
The remaining constants $A$ and $C$ are determined by the boundary conditions on the interface $y=0$.
Substituting in (\ref{eqn:wavesflina}-\ref{eqn:wavesflinkbc}) then yields a set of five coupled equations for $A$, $C$, $p_a$, $p_w$ and $h$ as
\begin{subequations}
\label{eqn:wavesslin}
\begin{align}
 p_a &= -\rho_a \left( \pd{~}{t} + ik U \right) A, \label{eqn:wavesslina}\\
 p_w &= -\rho_w \pd{C}{t}, \label{eqn:wavesslinw} \\
 p_a - p_w &= -(\rho_w - \rho_a) g h -\surft k^2 h, \label{eqn:wavesslinlaplace} \\
 \left( \pd{}{t} + ik U \right) h &= -|k| A, \label{eqn:wavesslinkbca} \\
 \pd{h}{t} &= |k|C. \label{eqn:wavesslinbcw}
\end{align}
\end{subequations}
Eliminating all the variables in favour of $h$ yields the master equation for our analysis
\begin{align}
\rho_w \pdd{h}{t} + \rho_a \left( \pd{~}{t} + ikU \right)^2 h + \left[(\rho_w-\rho_a) g |k| + \surft |k|^3 \right] h = 0.
 \label{eqn:wavesmaster}
\end{align}
Special cases of this equation demonstrate the various phenomena of waves and instabilities that interest us.

\section{Waves and instabilities}
We will start with deep water gravity waves to illustrate the solution and the use of the master equation \eqref{eqn:wavesmaster}.

\subsection{Deep water gravity waves} \label{sec:gravitywaves}
Here we set the wind speed $U=0$ and the surface tension $\surft =0$.
Doing so reduces \eqref{eqn:wavesmaster} to
\begin{align}
 \underbrace{(\rho_w+\rho_a) \pdd{h}{t}}_\text{\makebox[3.5cm]{Composite inertia}} + \underbrace{\vphantom{\pdd{h}{t}} (\rho_w-\rho_a) g |k| h}_\text{\makebox[3.5cm]{gravitational restoring force}} = 0.
\end{align}
This is a second order constant coefficient ordinary differential equation, which has the solution
\begin{align}
 h = E e^{i\omega t} + F e^{-i\omega t}, \quad \text{where} \quad \omega = \omega_\text{grav}(k) = \left[ \left(\dfrac{\rho_w-\rho_a}{ \rho_w + \rho_a}\right) g |k| \right]^{1/2},
 \label{eqn:wavesoscill}
\end{align}
where $E$ and $F$ are constants of integration, possibly functions of $k$.
Inverting the Fourier transform for $h$ and restoring the prime notation from \ref{eqn:wavesperturb} for the perturbation yields
\begin{align}
 h'(x,t) = \underbrace{\int_{-\infty}^\infty}_\text{linear combination} E(k) \underbrace{ \vphantom{\int_{-\infty}^\infty} e^{i(kx+\omega t)}}_\text{backward wave} + F(k) \underbrace{\vphantom{\int_{-\infty}^\infty} e^{i(kx-\omega t)}}_\text{forward wave}~\dd k.
 \label{eqn:wavespropagation}
\end{align}
As the labels indicate, the term proportional to $E$ and $F$ represent backward and forward propagating sinusoidal waves, respectively, and the integral in the inverse Fourier transform may be interpreted as forming a linear combination of sinusoids with all wavenumbers.
The functions $E(k)$ and $F(k)$ may be determined using knowledge of the initial conditions, say $h(x,t=0)$ and $\pd{h}{t}(x,t=0)$.

In other words, $E(k)$ and $F(k)$ are the amplitudes of the sinusoids that compose the initial condition. 
The sinusoids evolve independent of each other, which amounts to them propagating without changing shape.
The phase speed of wave propagation $c(k)$ depends on the wavenumber and is given by
\begin{align}
 c_\text{grav}(k) = \dfrac{\omega(k)}{k} = \left[ \left(\dfrac{\rho_w-\rho_a}{ \rho_w + \rho_a}\right) \dfrac{g}{|k|} \right]^{1/2}.
 \label{eqn:gravitydispersion}
\end{align}
The relation $c(k)$, or equivalently $\omega(k)$, is known as the dispersion relation because it quantifies how waves of different wavenumbers that make the initial condition disperse.
At any subsequent time, the shape of the interface can be reconstructed by linearly combining the translated sinusoids. 
These are the dynamics of small amplitude waves, specifically in this case the deep-water gravity waves.
These waves are dispersive because different wavenumbers propagate at different speeds.
In particular, the phase speed decreases with increasing $|k|$, as shown in Figure~\ref{fig:Dispersion}. 
\begin{figure}[ht]
\centerline{\includegraphics{Dispersion}}
\caption{Dispersion relation for gravity, capillary and gravity-capillary waves.}
\label{fig:Dispersion}
\end{figure}

A simple dimensional argument rationalizes the propagation speed of deep-water gravity waves.
For a sinusoidal wave with amplitude $h$ and wavelength $\lambda = 2\pi/k$, the excess weight of the fluid above the interface and the buoyancy below the interface (wavy shaded region in Figure~\ref{fig:WavesMechanism})  scales as $(\rho_w-\rho_a) g \lambda h$ per unit distance perpendicular to the plane of the page.
This unbalanced force drives motion in the surrounding fluid, the motion permeates a distance of $\mathcal{O}(\lambda)$ perpendicular to the interface, decaying further away from it, as shown by the translucent region in Figure~\ref{fig:WavesMechanism}.
Thus, the mass of the accelerated fluid scales as $(\rho_a + \rho_w) \lambda^2$ per unit width perpendicular to the plane of the page. 
The acceleration of this mass caused by the unbalanced force scales as $h/T^2$, where $T$ is the time-scale over which motion occurs, which is to be determined.
Balancing this mass times the acceleration with the unbalanced force yields
\begin{align}
 (\rho_a + \rho_w) \lambda^2 \times \dfrac{h}{T^2} \sim (\rho_w-\rho_a) g \lambda h \quad \Longrightarrow \quad c \propto \dfrac{1}{T} \sim \left[ \left( \dfrac{\rho_w-\rho_a}{\rho_w + \rho_a} \right) \dfrac{g}{\lambda} \right]^{1/2}.
 \label{eqn:gravdimdisp}
\end{align}
In other words, the wave period scales inversely with $\lambda^{1/2}$ because the accelerated mass scales with $\lambda^2$ but the driving force scales proportional to $\lambda$. 
The dimensional dependence on the difference and sum of the density is also rationalized in this simple analysis.

\subsection{Capillary and gravity-capillary waves}
In this case, we set $U=0$ for deep water gravity-capillary waves, and $g=0$ in addition for capillary waves.
Repeating the analysis of \S\ref{sec:gravitywaves}, yields 
\begin{align}
\omega(k) = \left[ \left(\dfrac{(\rho_w-\rho_a) g |k| + \sigma |k|^3}{ \rho_w + \rho_a}\right) \right]^{1/2} \quad \text{and} \quad c(k) = \left(\dfrac{(\rho_w-\rho_a) \dfrac{g}{|k|} + \sigma |k|}{ \rho_w + \rho_a}\right)^{1/2},
\label{eqn:gravcapdispersion}
\end{align}
for the gravity-capillary waves.
In the capillary limit, these expressions become
\begin{align}
\omega_\text{cap}(k) = \left(\dfrac{\sigma |k|^3}{ \rho_w + \rho_a}\right)^{1/2} \quad \text{and} \quad c_\text{cap}(k) = \left(\dfrac{\sigma |k|}{ \rho_w + \rho_a}\right)^{1/2}.
\label{eqn:capdispersion}
\end{align}
A dimensional analysis interpretation of \eqref{eqn:capdispersion} proceeds on the same lines as the derivation of \eqref{eqn:gravdimdisp} except for the restoring force of gravity $(\rho_w-\rho_a) g \lambda$ being replaced by the Laplace pressure acting over length $\lambda$, i.e. $(\sigma h /\lambda^2) \times \lambda$.
For capillary waves, the phase speed increases with the wavenumber $|k|$, as shown in Figure~\ref{fig:Dispersion}.

\begin{figure}
\centerline{\includegraphics{WavesMechanism}}
\caption{A sketch explaning the dimensional rationalization for the dispersion relation \eqref{eqn:gravitydispersion}. The wavy shaded region shows the excess fluid weight or buoyant force. The extent of the motion of the fluid arising from the wave propagation is shown in red square. The motion decays away from the interface as shown by the gray shading.}
\label{fig:WavesMechanism}
\end{figure}

Now we turn to some salient features of gravity-capillary waves as characterized by the dispersion relation.
The expressions for the phase speeds of wave propagation for gravity, capillary, and gravity-capillary waves are plotted in Figure~\ref{fig:Dispersion}.
The combination of gravity and capillary forces acting on the interface has complementary effect on the dispersion relation for long and short waves.
On the one hand, the force of capillarity is strong for short waves, i.e. waves with short wavelength corresponding to large $k$, and therefore the gravity-capillary dispersion relation approaches that of capillary waves in the large-$k$ limit.
In this regime, $c(k)$ increases with $k$.
On the other hand, the gravitational restoring force dominates for long wave, i.e. waves with long wavelengths corresponding to small $k$, and the gravity-capillary dispersion relation approaches that of gravity waves in that limit.
In this regime, $c(k)$ decreases with $k$.
For intermediate values of $k$, the gravity-capillary phase speed has a minimum, which can be found by setting
\begin{align}
 \dfrac{d}{d|k|} ( c(k)^2 ) = \dfrac{1}{(\rho_w +\rho_a)} \left[ \sigma - \dfrac{(\rho_w-\rho_a) g}{|k|^2}\right] = 0 \quad \Longrightarrow \quad |k| = k_{*} = \left[ \dfrac{(\rho_w-\rho_a) g}{\sigma} \right]^{1/2},
\end{align}
for which
\begin{align}
 c(k_{*}) = c_{*} = \dfrac{\left[4 (\rho_w-\rho_a) g \sigma\right]^{1/4}}{(\rho_w + \rho_a)^{1/2}}.
\end{align}

To get a sense of what this calculations represent, let us substitute some numbers into the expressions.
The density of water $\rho_w=10^3$ kg/m$^3$ is much greater than the density of air $\rho_a = 1.2$ kg/m$^3$, so $\rho_w \pm \rho_a \approx \rho_w$.
Gravity $g = 9.8$ m/s$^2$ and surface tension $\surft = 72 \times 10^{-3}$ N/m.
The slowest propagating wave corresponds to wavenumber
\begin{align}
 k_* = \left[ \dfrac{(\rho_w-\rho_a) g}{\sigma} \right]^{1/2} \approx 368 \text{ m}^{-1}.
\end{align}
This corresponds to a wavelength of 
\begin{align}
 \lambda_* = \dfrac{2\pi}{k_*} = 1.7 \text{ cm}.
\end{align}
Waves on the ocean surface with wavelength much larger than $\lambda_*$ are gravity waves governed by the part of the dispersion relation that approximates \eqref{eqn:gravitydispersion}, whereas waves with wavelength much shorter than $\lambda_*$ are governed by \eqref{eqn:capdispersion} dominated by capillarity.
The speed of the slowest propagating wave is
\begin{align}
 c_* = 23 \text{ cm/s}.
\end{align}
We shall refer to these numerical values later in this chapter.

\begin{figure}
 \centerline{\includegraphics[width=10cm]{Behroozi2006_CapGravDispersion}}
 % \vspace{3cm}
 % \centerline{\fbox{Figure awaiting permission.}}
 % \vspace{3cm}
\caption{Experimentally measured dispersion relation for the air-water interface compared with theory. The black dots are experimentally measured values of phase speed of sinusoidal waves and the solid curve is from \eqref{eqn:gravcapdispersion} with values substituted for the air-water interface at 20$^\circ$ C. Reproduced from \href{https://doi.org/10.1119/1.2215617}{Behroozi and Perkins, American Journal of Physics, {74}(11), pp. 957-961}with the permission of the American Association of Physics Teachers.}
 \label{fig:Behroozi2006_CapGravDispersion}
\end{figure}

Even though the mathematical analysis so far has been based on vanishingly small perturbation amplitude, the results of this theory apply quite well to practical situations where the amplitude of the waves is small compared to its wavelength, as shown in Figure~\ref{fig:Behroozi2006_CapGravDispersion}. However, it is not merely the applicability of the analysis to the corresponding physical system that is of value, but also the fundamental concepts of wave propagation and the insight into the physical processes, which cannot be gained purely from an experimental approach. This insight ties into the subsequent analysis on instabilities and thus provides a more complete picture of the underlying physics than possible from analyses of the phenomena considered separately.

\subsection{Rayleigh-Taylor instability}
We now turn to the mechanism of overturning of the interface when the fluid above is heavier than the fluid below.
The situation is analogous to balancing an inverted pendulum.
While theoretically a pendulum may be balanced in an inverted configuration by having the point of support {\em exactly} below the centre of gravity of the pendulum, it is practically impossible to achieve it.
The smallest, even infinitesimal, perturbation from a systematic or a random source in the environment, pushes the pendulum out of balance and the ensuing dynamics naturally pushes it further away from the equilibrium.
A similar dynamics occurs when attempting to balance a semi-infinite layer of heavier fluid on top of a semi-infinite layer of lighter fluid.
While the governing equations allow such a static equilibrium to exist, even an infinitesimal perturbation away from this equilibrium drives the interface away from its flat shape.
The resulting fluid flow causes an overturning of the interface, causing the heavier fluid to flow downwards and pushs the lighter fluid upwards.
The mechanism of this dynamics is termed as the Rayleigh-Taylor instability, after Lord Rayleigh and Geoffrey Ingram Taylor, who first presented the essential aspects of the dynamics.

A simple way to represent heavy fluid on top of light is to transform $g \to -g$ and set $U=0$ in \eqref{eqn:wavesmaster}. (Equivalently, we can switch $\rho_w$ and $\rho_a$, the only parameters that depend on the fluids.)
\begin{align}
(\rho_w+\rho_a) \pdd{h}{t} + \left[ \surft |k|^3 - (\rho_w-\rho_a) g |k|\right] h = 0.
 \label{eqn:wavesmasterrt}
\end{align}
The corresponding dispersion relation 
\begin{align}
\omega(k) = \left[ \left(\dfrac{\sigma |k|^3 - (\rho_w-\rho_a) g |k| }{ \rho_w + \rho_a}\right) \right]^{1/2} \quad \text{and} \quad c(k) = \left[\dfrac{\sigma |k| - (\rho_w-\rho_a) \dfrac{g}{|k|} }{ \rho_w + \rho_a}\right]^{1/2}.
\label{eqn:rtdispersion}
\end{align}
Equation \eqref{eqn:rtdispersion} only differs from \eqref{eqn:gravcapdispersion} by a negative sign, which changes the qualitative character of the dynamics.
For short waves, $|k|>k_*$, the numerator inside the square-root for $\omega(k)$ is positive, and the wave-propagation character of the dynamics is preserved just as for gravity-capillary waves, albeit with a modified wave speed.
But for long waves, $|k|<k_*$, the numerator is negative and $\omega(k)$ is purely imaginary.
This implies that the oscillatory behaviour of $e^{\pm i \omega t}$ in \eqref{eqn:wavesoscill} changes to exponential growth or decay, and the wave propagation at constant speed in \eqref{eqn:wavespropagation} changes to exponential growth or decay of sinusoids. 

We will now interpret the mathematical analysis in term of its physical mechanics.
According to hydrodynamic stability theory, perturbations are unavoidable in any physical system.
When such a perturbation is forced on the interface, it can be decomposed into the normal modes of the system.
As described in \eqref{eqn:wavespropagation}, $\eps E(k)$ and $\eps F(k)$ are the infinitesimally small amplitudes of sinusoids with wavenumber $k$ comprising the initial perturbation to the interface shape.
Each of these modes evolve independently. 
If $\omega$ is purely imaginary, $e^{i\omega t}$ decays exponentially, so the sinusoid proportional to $F(k)$ decays exponentially, and thus its magnitude remains infinitesimal.
But $e^{-i \omega t} $ grows exponentially, so the sinusoid proportional to $E(k) $ grows in magnitude, at least until the perturbation is no longer infinitesimal.
Generically, perturbations forced randomly by the environment contain all normal modes of the system. 
Most of them are expected to decay, but if even a single mode grows exponentially, this growth continues until the flat interface shape is disrupted.
The shape of the interface then more and more closely resembles the shape of the fastest growing mode.
Therefore, the analysis of hydrodyanamic stability reduces to the determination of growing normal modes.

For Rayleigh-Taylor instability, the exponential growth rate is given by
\begin{align}
 i\omega = \left[ \left(\dfrac{(\rho_w-\rho_a) g |k| - \surft |k|^3}{ \rho_w + \rho_a}\right) \right]^{1/2}, \quad 0\leq k \leq k_*.
\end{align}
The growth rate is zero at $k=0$ and $k=k_*$, and positive in between.
The growth is fastest for wavenumber given by
\begin{align}
 |k| = \left[\dfrac{(\rho_w-\rho_a) g}{3\sigma} \right]^{1/2} = \dfrac{k_*}{\sqrt{3}}.
\end{align}
This is the fastest growing mode.

Physically, a depression in the interface displaces heavy fluid downwards with lighter fluid surrounding it.
The lighter fluid does not provide sufficient buoyancy to compensate for the weight of the heavy fluid.
Similarly, if a parcel of the lighter fluid is displaced upwards, the buoyant force on it exceeds its weight.
The imbalance in both these situations continues to push the heavier fluid downward and the lighter fluid upwards.
For short waves, the tension in the surface provides sufficient retoring force to overcome the driving force of gravity, but the process runs away for long waves overturning the interface.
The dimensional analysis version of Rayleigh-Taylor closely parallels that of gravity-capillary waves, except gravity provides a driving force destabilizing this static flat interface instead of a restoring force.
This is the physical mechanism of the Rayleigh-Taylor instability.

\subsection{Kelvin-Helmholtz instability}
In this case, we restore the blowing wind and retain every variable in \eqref{eqn:wavesmaster}.
The interpretation is facilitated by the substitution $(\rho_w-\rho_a)g |k| + \surft |k|^3 = (\rho_w + \rho_a) c^2 k^2$ from \eqref{eqn:gravcapdispersion}, which leads to the the form of \eqref{eqn:wavesmaster} as
\begin{align}
 \rho_w \pdd{h}{t} + \rho_a \left( \pd{h}{t} + ikU \right)^2 h + (\rho_w + \rho_a) c^2 k^2 h = 0.
\end{align}
The dispersion relation for this equation is the solution of
\begin{align}
 \rho_w \omega^2 + \rho_a (\omega + kU)^2 = (\rho_w + \rho_a) c^2 k^2. 
\end{align}
This is a quadratic equation for $\omega$, which has the solution
\begin{align}
 \dfrac{\omega}{k} = - \dfrac{\rho_a U}{\rho_w+\rho_a} \pm \sqrt{c^2 - \dfrac{U^2 \rho_w\rho_a}{(\rho_w+\rho_a)^2}}.
\end{align}
The variable $\omega$ is real for small $U$ if discriminant inside the square-root is positive, and sinusoids propagate at a constant speed $\omega/k$.
However, for 
\begin{align}
 U > U_\text{cr}(k) = \left(f + f^{-1} \right) c(k), \quad f = \sqrt{\dfrac{\rho_w}{\rho_a}},
\end{align}
the discriminant is negative, $\omega$ is complex, and sinusoids grow exponentially.
Here $U_\text{cr}(k)$ is the critical wind speed, which when exceeded perturbation to the interface with wavenumber $k$ is amplified by the wind.
The smallest value for $U_\text{cr}$ that leads to amplification of any wavenumber occurs for $k=k_*$, where $c(k)$ has its minimum value of $c_*$.
This analysis makes the following physical prediction: on a windy day as the gusts pick up, the first appearance of ripples spontaneously appearing on the interface corresponds to a wavenumber $k=k_*$, wavelength $\lambda=\lambda_*$ at a wind-speed 
\begin{align}
U=U_\text{cr}(k_*) = \left(f + \dfrac{1}{f} \right) c_*.
\end{align}
For the air-water interface, using $\rho_a \ll \rho_w$, this speed is
\begin{align}
 U \approx \dfrac{(4 \rho_w g \surft)^{1/4}}{\rho_a^{1/2}} = 6.7 \text{ m/s}.
\end{align}

\begin{figure}
 \centerline{\includegraphics{InstabilityMechanism}}
 \caption{Instability mechanism for Kelvin-Helmholtz instability. In the reference frame of the wave, the wind speed is $U-c$. The air flowing over the crests speed up because it has to flow through smaller space than in the absence of the wave (blue arrows). By the Bernoulli principle, the air pressure there drops and pulls the interface futher away from the flat shape (red arrows). Similarly, through the troughs, the flow slows down and the pressure rises, further pushing the troughs to become deeper.  }
 \label{fig:InstabilityMechanism}
\end{figure}

To physically understand the mechanism underlying this mathematical analysis of exponential growth, we go back to the expression for the air pressure in \eqref{eqn:wavesslina} and \eqref{eqn:wavesslinkbca}.
Writing the air pressure in terms of interface perturbation gives
\begin{align}
 p_a = \dfrac{\rho_a}{|k|} \left( \pd{}{t} + ikU \right)^2 h.
\end{align}
To imagine the influence of the blowing wind on a propagating wave, we substitute $h \propto a e^{i(kx-kct)}$ (here $c$ is the speed of propagation of the sinusoid and $a$ its amplitude) to yield the air pressure to be
\begin{align}
 p_a \propto -\rho_a | k| \left(U - c\right)^2 a e^{i(kx-kct)}.
\end{align}
When air flows over the crests of the waves, it has the squeeze through a narrower space because of presence of the crest reduces the space available for the air to flow.
This results in a speeding up when flowing over the crests, as shown in Figure~\ref{fig:InstabilityMechanism}.
Similarly, the wind slows down when flowing through the troughs, also shown in Figure~\ref{fig:InstabilityMechanism}.
By the Bernoulli equation, the pressure is lower where the speed is faster, i.e. the crests, and higher where the speed is slower, i.e. the troughs.
This influence of the air pressure out-of-phase with the waveform acts to amplify the interface waveform and drives the instability.

It so happens that the neglect of viscosity is tolerable for the waves and the Rayleigh-Taylor instability. 
However, the uniform wind speed violates the no-slip condition on the interface.
The presence of viscosity smoothes this profile, and the effect of this modified velocity profile needs to be accounted for in the stability analysis.
This is the subject for further research for the keen student.
