\chapter{Hydrostatics and Bernoulli equation}
In this chapter, we will treat two closely related topics in fluid mechanics -- hydrostatics and the Bernoulli equation.
The reader is presumably acquainted with the concepts in hydrostatics; here we include it for completeness and to illustrate the proofs underlying some celebrated principles.
The Bernoulli equation is one of the most important results in fluid dynamics because, unlike the Navier-Stokes equations, it yields answers to questions inspired by applications.

\section{Hydrostatics}
Hydrostatics is the study of the mechanics of static fluids. In this case $\bu=0$ and the Navier-Stokes equations reduce to
\begin{align}
 \grad p = \rho \bg,
\end{align}
which may be readily integrated to yield
\begin{align}
p = p_0 + \rho \bg\cdot (\bx -\bx_0),
\end{align}
where $p_0$ is the pressure at a reference point $\bx_0$. The general result is that pressure increases with depth at a rate that balances the increase of weight of the fluid per unit area.

\subsection{Archimedes principle}
A cornerstone of hydrostatics is force exerted by a static fluid on immersed (or floating) bodies given by the Archimedes principle. We can now derive this principle for a body occupying a volume $\Omega$ as follows. The net hydrostatic force on the body, $\bF_h$, is due to hydrostatic pressure
\begin{align}
 \bF_h = \int_{\partial\Omega} -p \nhat~dA = \int_{\partial\Omega} \left( p_0 + \rho \bg\cdot (\bx -\bx_0) \right) \nhat~dA,
\end{align}
where $\nhat$ is the unit normal pointing out of the body. This expression can be simplified using the divergence theorem as
\begin{align}
 \bF_h = \int_\Omega \rho \bg~d\Omega.
\end{align}
In other words, the net force on the immersed body is equal to the net volumetric force acting on the fluid displaced by the body. This is the statement of Archimedes principle.

\section{Bernoulli equation}
The Bernoulli equation is a celebrated result in fluid dynamics, which could be considered as a generalization of hydrostatics. There are two different conditions for incompressible flow under which two different versions of Bernoulli equation apply. Here we present those two. Analogues of these equations for compressible flow are also possible, which we do not treat.

Underlying both of these versions is an expression of incompressible momentum balance in differential form that uses the identity
\begin{align}
 \bu \times \bomega = \bu \times (\grad\times\bu) = \grad \left(\dfrac{1}{2} | \bu | ^2 \right) - \bu \cdot\grad\bu.
 \label{eqn:identity}
\end{align}
Using \eqref{eqn:identity} to re-write the $\bu\cdot\grad\bu$ term in the momentum balance yields
\begin{align}
 \left( \rho \pd{\bu}{t} + \grad \left(\dfrac{1}{2} \rho | \bu | ^2 + p - \rho \bg\cdot\bx \right) \right) = \mu \nabla^2 \bu + \rho \bu \times \bomega.
 \label{eqn:velvort}
\end{align}

\subsection{Steady incompressible inviscid flow}
For a steady inviscid flow, \eqref{eqn:velvort} simplifies to
\begin{align}
\grad \tilde{B}= \rho \bu \times \bomega, \qquad \text{where} \quad \tilde{B} =   \dfrac{1}{2} \rho | \bu | ^2 + p - \rho \bg\cdot\bx 
\end{align}
The derivative of $\tilde{B}$ in the direction of either $\bu$ or $\bomega$ is zero, i.e.,
\begin{align}
 \bu \cdot\grad \tilde{B} = \rho \bu \cdot (\bu \times \bomega) = 0 \text{ and } \bomega \cdot\grad \tilde{B} = \rho \bomega \cdot (\bu \times \bomega) = 0.
\end{align}
Therefore, $\tilde{B}$ is constant along a curve that is everywhere tangent to the velocity (i.e. along a streamline) or to the vorticity (i.e. along a curve known as the vortex line). 
\subsection{Unsteady incompressible potential flow} \label{sec:unsteadybernoulli}
Potential flow implies that the velocity is the gradient of a scalar function, $\phi(\bx, t)$, called the vector potential, i.e., $\bu = \grad\phi$, everywhere in the domain of interest. Due to incompressibility, the velocity potential satisfies the Laplace equation
\begin{align}
 \grad\cdot\bu = 0 \quad \Longrightarrow \quad \grad\cdot(\grad\phi) = \nabla^2 \phi = 0. \label{eqn:vanishvisc}
\end{align}
Furthermore, owing to the identity that the curl of a gradient vanishes, 
\begin{align}
\bomega = \grad\times\bu = \grad\times \grad\phi = 0. \label{eqn:vanishvort}
\end{align}
The two terms on the right-hand side of \eqref{eqn:velvort} vanish because $\nabla^2 \bu = \nabla^2 (\grad\phi) = \grad (\nabla^2\phi) = \boldsymbol{0}$, and $\bomega=0$. The unsteady term on the left hand side can also be included inside the gradient to yield
\begin{align}
 \grad \hat{B} = \boldsymbol{0}, \qquad \text{where} \qquad \hat{B} = \rho \pd{\phi}{t} + \dfrac{1}{2} \rho | \bu | ^2 + p - \rho \bg\cdot\bx.
 \label{eqn:Bernoulliunsteady}
\end{align}
Because the gradient of $\hat{B}$ vanishes everywhere, $\hat{B}$ does not vary with the spatial coordinates, but may depend on time
Note that there is no condition on the viscosity of the fluid in this case.

\section{Applications and examples}
The main use of Bernoulli equation is the determine pressure once the form of velocity is known.
In the discussion surrounding the Navier-Stokes equations (see \S\ref{sec:idealfluid}), the difficulty of determining pressure for an incompressible flow was identified. In general, for incompressible fluids, pressure is a variable that is determined implicitly through the condition that the fluid density is not altered by the flow. 
This difficulty is overcome by the Bernoulli equation when one of the conditions for its validity applies. 
Also note that the two forms of Bernoulli equation reduce to hydrostatics for the case $\bu \equiv \boldsymbol{0}$.
Here we will present examples of the application of Bernoulli equation.

In many cases, the application of Bernoulli in the steady inviscid case does not require an explicit specification of the complete velocity profile. Instead, the velocity is known at some specific location, and use is made of it to derive relations of practical use.

\subsection{Flow through an opening in a tank}
Consider water flowing out of a tank through a opening of aera $A$, which is small relative to the area of the tank viewed from above, as shown in Figure~\ref{fig:Spout}, thereby draining the tank. The question is to determine the rate at which the tank empties. We will see that the rate of emptying will depend on the level of water, $H$, above the level of the opening.

Here note that the flow is not steady. This is because as the water-level falls, the driving force $H$ decreases, thereby reducing the flow with time. But if the water-level doesn't fall too fast, for example, the opening being too small to drain the water, then a steady approximation might be worthwhile to make.  Nor is water inviscid, but we imagine considering the limit of the fluid being inviscid to examine what the consequence of that assumption would be. If the consequence of these approximations agrees with observations, then we will deduce that the assumptions are useful. So let's get on with it.

We are not given any details of the velocity field. So we will assume the velocity to be uniform, with a value $U$, across the area of the opening. Now let's trace back a streamline from some point near the opening to another point where we might be able to approximate the velocity and pressure. Note that, by the definition of a streamline, it depicts the instantaneous chain of displacement of fluid particles. Since the fluid exiting the opening is ultimately being displaced by the falling water-level, every streamline that starts in the opening must terminate at the free surface. Thus we consider a streamline $BB'$ or $CC'$ for our consideration.

On such a streamline, Bernoulli equation for steady inviscid flow states
\begin{align}
\tilde{B} = \dfrac{1}{2} \rho | \bu | ^2 + p - \rho \bg\cdot\bx = \text{constant},
\end{align}
and, therefore,
\begin{align}
 p_B + \dfrac{1}{2} \rho U^2 - \rho \bg\cdot\bx_B = p_{B'} +  \dfrac{1}{2} \rho U_{B'}^2 - \rho \bg\cdot\bx_{B'}.
\end{align}
Here we use our approximation $U_{B'} \ll U$, so the term $\rho U_{B'}^2/2$ on the right-hand side is negligible. We also use that both $p_B$ and $p_{B'}$ are close to atmospheric values (because both the free surface and the opening are exposed to open air), so they cancel each other from the two sides of this equation. Then using $\bg\cdot(\bx_B - \bx_{B'}) = gH$ and solving for $U$ yields
\begin{align}
 U = \sqrt{2gH}.
\end{align}
Water exits the opening at the same speed as an object freely falling under gravitational acceleration $g$ from a height $H$!

\begin{figure}
\centerline{\includegraphics{Spout}}
\caption{Schematic setup for flow out of an opening in a tank.}
\label{fig:Spout}
\end{figure}

\subsection{A wind turbine}
A wind turbine is a device that extracts the kinetic energy from an otherwise uniform flow of a fluid, in this case air. 
Due to the law of conservation of energy, the fluid in the wake of the turbine must lack in kinetic energy flux. 
The turbine must necessarily exert a drag on the flow to slow it down. 
Without this drag, the flow in the wake of the turbine could not be retarded to generate a deficit in the kinetic energy flux.
A simple calculation reveals that there exists an optimum amount of drag for a turbine to extract the most power from the flow. 

This calculation is based on the abstraction of the wind turbine as the so-called ``linear momentum actuator disc'', or simply an ``actuator disc''.
An actuator disc is a thin disc-shaped region of space where a concentrated force acts. 
An example of an actuator disc, in the context of analyzing the response of a wind turbine, is shown in Figure~\ref{fig:Windmill2}. 
The region swept by the blades of the turbine is the actuator disc; the region is called a disc because it is thin along the streamwise direction. 
As an idealization, the influence of the turbine on the flow is replaced by a concentrated body force opposing the flow exerted on the fluid occupying this disc.
While the force exerted by the blades of a real turbine on the fluid is unsteady (because of the rotation of the blades), the unsteadiness is ignored in the idealization and replaced by a steady force uniformly distributed throughout the disc.
In practice, as the turbine spins faster, the greater the average force it exerts on the fluid. (This is similar to the operation of a fan -- the faster the blades of the fan spin the greater the force it exerts on the surrounding fluid and faster the resulting flow.)

Now we ask, how much force should the turbine exert on the flow. 
If the exerted force is too weak then the kinetic energy deficit in the wake, and thereby the power extracted, is also small.
But if the exerted force is too strong then the turbine acts as a fan and blows the fluid backwards thus pumping power back into the flow, instead of extracting it from the flow.
Hence, there must be an optimal amount of force to exert on the fluid to maximize power extraction.
This principle can be understood quantitatively using the actuator disc idealization for the turbine which exerts a steady force on the fluid.

\begin{figure}
 \centerline{\includegraphics{Windmill2}}
 \caption{Depiction of the actuator disc idealization of a wind turbine. The blue shaded region at ``Station 2'' swept by the blades of the model turbine is the actuator disc. The stream tube flowing through the actuator disc is also shown bounded by the black curves. The brown shaded region shows the stream tube at ``Station 1'', which is far upstream on the turbine, and the green shaded region at ``Station 3'' far downstream. The fluid speed at ``Station $i$'' is $U_i$.}
 \label{fig:Windmill2}
\end{figure}

The problem is stated as follows. A turbine of radius $R$ operates in an incompressible fluid of density $\rho$ flowing uniformly at speed $U$. What is the maximum power, $P$, it can extract from the flow? We do not anticipate the fluid viscosity to be a dominant effect. It then follows from dimensional analysis that
\begin{align}
 P = \eta \times \dfrac{1}{2} \rho U^3 \pi R^2,
\label{eqn:wmnondimpower}
\end{align}
where $\eta$ is a dimensionless number called the efficiency of the turbine. The efficiency is interpreted to be the fraction of the kinetic energy flux $\dfrac{1}{2} \rho U^3 \pi R^2$ incident on the swept area of the turbine blades that the turbine can convert to power. Similarly, the drag is 
\begin{align}
D = C_D \times \dfrac{1}{2} \rho U^2 \pi R^2,
\label{eqn:wmnondimdrag}
\end{align}
where $C_D$ is the coefficient of drag. 
According to the argument in the previous paragraph, the efficiency is expected to be maximum for a particular drag coefficient.

Let us now construct estimates of the efficiency and the drag coefficient, which happens to be convenient to do in terms of the wake deficit.
Consider three stations on the stream tube incident on the turbine, shown in Figure~\ref{fig:Windmill2} -- Station 1 far upstream of the turbine, Station 2 at the turbine, and Station 3 far downstream.
The fluid velocity at Station 1 is expected to be uniform, with a magnitude equal to the given freestream $U$, so $U_1=U$.
Similarly, the fluid velocity at Station 3 is also expected to be uniform in the stream tube -- we will take the speed to be $U_3$. (Outside the streamtube, far downstream, the fluid speed is $U$.)
If the turbine extracts power from the flow, we expect $U_3 < U$.
The cross-section area of the stream tube at Stations 1, 2 and 3, is taken to be $A_1$, $A_2$ and $A_3$, respectively, with $A_2 = \pi R^2$ given.
The fluid pressure at Stations 1 and 3 are equal to the atmospheric value $p_\infty$.

Now we apply laws of conservation of mass and momentum in integral form to the volume $\Omega$ occupied by the stream tube.
According to the law of conservation of mass
\begin{align}
 U_1 A_1 = U_2 A_2 = U_3 A_3.
 \label{eqn:wmmass}
\end{align}
Momentum conservation between Stations 1 and 3 implies
\begin{align}
 \rho U_1^2 A_1 - \rho U_3^2 A_3 + \int_{\partial\Omega} (p-p_\infty)\nhat~dA - D = 0.
\end{align}
It is left as an exercise for the reader by considering mass and momentum balance on the fluid outside $\Omega$ to show that the pressure integral $\int_{\partial\Omega} (p-p_\infty)\nhat~dA = 0$, thereby
\begin{align}
D = \rho U_1^2 A_1 - \rho U_3^2 A_3 = \rho U_2 A_2 ( U_1-U_3).
\label{eqn:wmdragmom}
\end{align}
Similarly, mechanical energy conservation implies
\begin{align}
P = \dfrac{1}{2} \rho U_1^3 A_1 - \dfrac{1}{2} \rho U_3^3 A_3 = \dfrac{1}{2} \rho U_2 A_2 \left( U_1^2-U_3^2 \right).
\label{eqn:wmpower}
\end{align}
Note that no characteristics of the turbine are used to derive these relations.

It is now time to relate these results from the integral form of the conservation laws to the size of the turbine itself.
To facilitate the subsequent analysis, consider the profiles of velocity and pressure along the centerline of the stream tube, which also happens to be a streamline due to axisymmetry.
These profiles are shown schematically in Figure~\ref{fig:WindmillProfiles}.
The fluid speed far upstream of the turbine is $U_1=U$, but under the influence of the drag exerted by the actuator disc, it decreases monotonically along the streamline to $U_2$ at the turbine and to $U_3$ far downstream.
On the streamlines upstream of the turbine and downstream of the turbine, the fluid pressure profile obeys the Bernoulli equation.
Therefore, upstream of the turbine, the pressure grows monotonically, starting with $p_1 = p_\infty$ far upstream. 
At the turbine itself, because of the drag on the actuator disc, the pressure profile suffers a jump.
Downstream of the turbine, the pressure again grows monotonically to $p_3=p_\infty$ far downstream.
\begin{figure}
 \centerline{\includegraphics{WindmillProfiles}}
 \caption{Schematic of profiles of speed and pressure along the streamline shown in Figure~\ref{fig:Windmill2}.}
 \label{fig:WindmillProfiles}
\end{figure}

The jump in the pressure across the actuator disc may be determined by applying the Bernoulli equation along the streamline between Stations 1 and 2, and 2 and 3, respectively, which reads
\begin{align}
 p_1 + \dfrac{1}{2} \rho U_1^2 = p_2^- + \dfrac{1}{2} \rho |U_2|^2, \\
 p_3 + \dfrac{1}{2} \rho U_3^2 = p_2^+ + \dfrac{1}{2} \rho |U_2|^2.
\end{align}
Here we have tacitly assumed that the component of fluid velocity tangential to the actuator disc does not suffer a jump at Station 2. 
\mynote{This assumption remains unstated in many treatments of this topic.}
(Note that the normal component must be continuous owing to mass conservation.) 
The difference between the two yields
\begin{align}
 p_2^- - p_2^+ = \dfrac{1}{2} \rho \left(U_1^2 - U_3^2 \right). 
\end{align}
Note that the magnitude of the pressure jump across the actuator disc depends only on the far upstream and far downstream speed on the streamline passing through any point on the actuator disc, and is therefore independent of the location on the actuator disc.
Applying conservation of momentum across the actuator disc then yields the drag on it to be
\begin{align}
 D = (p_2^- - p_2^+) A_2 = \dfrac{1}{2} \rho A_2 \left(U_1^2 - U_3^2 \right).
 \label{eqn:wmdragbern}
\end{align}
Equating the expressions for drag from \eqref{eqn:wmdragmom} and \eqref{eqn:wmdragbern} yields a celebrated equation on this topic
\begin{align}
 U_2 = \dfrac{U_1 + U_3}{2}.
 \label{eqn:wmrankine}
\end{align}
This equation states that at the actuator disc, the fluid speed is equal to the average of the far upstream and downstream values.

Now we are prepared to write the power extracted and the drag exerted by the actuator disc in terms of the wake deficit, which we parametrize by $U_2$. 
For convenience, we non-dimensionalize $U_2 = a U$ and $U_3 = b U$ so that \eqref{eqn:wmrankine} becomes
\begin{align}
 a = \dfrac{1+b}{2} \qquad \text{or} \qquad b = 2a-1.
\label{eqn:wmdimlessrankine}
\end{align}
The parameters $a$ and $b$ are called the turbine induction factor and the wake induction factor, respectively.
Based on this result, and that the minimum value of $b$ is zero, the minimum value of $a$ is 1/2.
In other words, the greatest deficit in the wake we consider is when the fluid in the wake comes to a complete stop, in which case the fluid speed at the turbine is half that of the freestream.

Substituting \eqref{eqn:wmdimlessrankine} in \eqref{eqn:wmdragmom} and \eqref{eqn:wmpower} yields
\begin{align}
 D = \rho U^2 \pi R^2 a (1-b) = \dfrac{1}{2}\rho U^2 \pi R^2 \times 4a(1-a) , \\
 P = \dfrac{1}{2} \rho U^3 \pi R^2 a (1-b^2) = \dfrac{1}{2} \rho U^3 \pi R^2 \times 4a^2 (1-a),
\end{align}
where by comparison with \eqref{eqn:wmnondimdrag} and \eqref{eqn:wmnondimpower}, the drag coefficient and efficiency are
\begin{align}
 C_D = 4 a (1-a), \qquad \text{and} \qquad \eta = 4 a^2(1-a).
\end{align}

\begin{figure}
\centerline{\includegraphics{WindmillEfficiency}}
\caption{Efficiency $\eta$ and drag coefficient $C_D$ as a function of the turbine induction factor $a$.}
\label{fig:WindmillEfficiency}
\end{figure}

The dependence of $\eta$ and $C_D$ on $a$ is shown in Figure~\ref{fig:WindmillEfficiency}. 
Both these variables are zero when $a=1$, because there is no wake-deficit in this case.
As $a$ decreases below unity, the wake deficit builds up, the turbine experiences drag and can extract some power.
The extracted power reaches a maximum at $a=a_{max}$, which may be found by setting
\begin{align}
 \dfrac{d\eta}{da} = 4(2a - 3a^2) = 0 \quad \Longrightarrow\quad a_\text{max} = \dfrac{2}{3} \quad \text{and} \quad \eta_\text{max} = \dfrac{16}{27} \approx 0.59.
\end{align}
Decreasing $a$ below $a_\text{max}$ reduces the extracted power more.
The drag coefficient that leads to maximum power extraction is $C_{D,\text{max}} = 8/9$.

This calculation introduces one of the most basic concepts to evaluate the performance of a wind turbine -- its efficiency.
Furthermore, no turbines have been invented with efficiency greater than 16/27.
This limit on the maximum efficiency of a linear momentum actuator disc is known as the 'Betz limit' or the 'Betz-Joukowski limit' commemorating the contributions of the German physicist Albert Betz and the Russian scientist and mathematician Nikolay Joukowski, for providing a detailed rationale behind its validity. 

\subsection{A water wave}

The Cartesian components of velocity $(u,v,w)$ in a semi-infinite layer of fluid ($y\le 0$) of density $\rho$ are given by 
\begin{align*}
u = k \sin (\omega t) \cos (kx) e^{-|k|y},\qquad v = -|k| \sin (\omega t) \sin (kx) e^{-|k|y},\qquad w=0,
\end{align*} 
where $\omega$ and $k$ are constants and $t$ is time. Gravity or any other volumetric force is absent. This velocity field closely approximates the flow under a sinusoidal train of water waves. The question is to deduce the pressure on the surface $y=0$. 

This is unsteady flow. It will be extremely useful if we could apply the unsteady version of Bernoulli equation here. Let us first examine if the conditions for it are satisfied.
\begin{enumerate}
 \item {\bf Is the flow incompressible?} 
 \begin{align}
  \grad\cdot\bu = \pd{u}{x} + \pd{v}{y} + \pd{w}{z} = \sin (\omega t) e^{-|k| y} \left(-k^2 \sin (kx) + |k|^2 \sin (kx) + 0 \right) = 0.
 \end{align}
 Yes!
 \item {\bf Is the flow irrotational?} The velocity is two-dimensional, so we only need to verify the $z$-component of vorticity
 \begin{align}
  \omega_z = \pd{v}{x} - \pd{u}{y} = \sin (\omega t) e^{-|k| y} \left(-k |k| \cos(kx) - (-k|k|) \cos(kx)  \right).
 \end{align}
 Yes! So the velocity may be written as the gradient of a potential $\phi$.
 \begin{align}
  \pd{\phi}{x} = u = k \sin (\omega t) \cos (kx) e^{-|k|y}, \quad \pd{\phi}{y} = v = -|k| \sin (\omega t) \sin (kx) e^{-|k|y},
 \end{align}
 which yields
 \begin{align}
  \phi = \sin (\omega t) \sin (kx) e^{-|k| y}.
 \end{align}
 According to Be
 rnoulli (in the absence of gravity), 
 \begin{align}
  p = - \rho \pd{\phi}{t} -\dfrac{1}{2} \rho |\bu|^2 = -\rho \omega \cos (\omega t) \sin (kx) e^{-|k|y} - \dfrac{1}{2} \rho k^2 \sin^2 (\omega t) e^{-2|k|y}. 
 \end{align}
 On $y=0$, the pressure is
 \begin{align}
  p|_{y=0} = -\rho \omega \cos (\omega t) \sin (kx) - \dfrac{1}{2} \rho k^2 \sin^2 (\omega t).
 \end{align}
 A useful homework is to deduce the pressure using the Navier-Stokes equations and verify that it agrees with the one obtained using Bernoulli.
\end{enumerate}
