\chapter{Acoustics and Incompressibility}
% \thispagestyle{fancy}
We have extensively employed the incompressible approximation despite being acutely aware of the fact that no physical material is exactly incompressible. In this chapter, we examine the justification underlying this assumption using dimensional analysis. The theory underlying the acoustic limit of Navier-Stokes equations is needed to support our analysis. Let us derive this theory.

\section{Acoustic limit of Navier-Stokes equations}
\begin{figure}[h]
\centerline{\includegraphics{AcousticSchematic}}
\caption{Schematic of the thought experiment to develop the acoustic approximation. A body force is applied to a thin slice of the fluid near the mid-plane for a short duration of time.}
\label{fig:AcousticSchematic}
\end{figure}
Consider the following thought experiment in a rectangular room filled with air. Imagine a fictitious agency exerting a body force concentrated on a thin slice around the mid-plane of the room for a short duration. Let us imagine the slice to be much thinner than the room length and the duration to be short. This setup is schematically shown in Figure~\ref{fig:AcousticSchematic}.

If the fluid were incompressible, the response of the fluid would simply be to be compressed in the right half of the room and be de-compressed in the left half for the duration of the pulse. However, for a fluid with properties similar to air, a computational solution obtained using COMSOL illustrates a different outcome. For this simulation, the ambient pressure was taken to be $p_0=1$ Pa and the ambient density to be $\rho_0=1$ kg/m$^3$. The equation of state is
\begin{align}
 p = p_0 \left(\dfrac{\rho}{\rho_0}\right)^\gamma,
 \label{eqn:state}
\end{align}
where $\gamma$ is the adiabatic gas index, which is equal to the ratio of specific heats of the gas under constant pressure and under constant volume. For air, $\gamma=1.4$ approximately. 
A body force impulse was applied to a slice of about 0.5 m around the mid-plane of a 20 m $\times$ 20 m room for about 10 ms. Figure~\ref{fig:SoundPressure} shows the snapshots of the fluid pressure that develop in the room every 2 seconds. Clearly, the fluid in the room does not behave according to our expectations for an incompressible fluid. Two pulses of pressure originate near the mid-plane. The high-pressure pulse propagates to the right and the low-pressure pulse to the left. When they encounter the walls of the room, they get reflected and the reflected pulses meet near the mid-plane. Clearly, this behaviour is similar to that of a wave, in this case a sound wave.

\begin{wrapfigure}[42]{l}{2.2in}
\includegraphics[height=8.5in]{SoundPressure}
\caption{Snapshots of pressure perturbation in millibars at 2 s intervals from $t=0$ (top) to $t=16$ s (bottom).}
\label{fig:SoundPressure}
\end{wrapfigure}
An acoustic limit of the Navier-Stokes equations explains the origins of sound propagation in compressible media and many features of the observations from the COMSOL simulation, such as the speed of propagation of the pulses. 

\subsection{The Acoustic limit}
To derive the acoustic limit of Navier-Stokes equations, we linearly perturb the inviscid compressible version of the equations to small amplitude motion. The inviscid compressible governing equations are
\begin{align}
\pd{\rho}{t} + \grad\cdot(\rho \bu) &= 0, \label{eqn:mass} \\
\rho \left( \pd{\bu}{t} +\bu \cdot\grad\bu \right) &= -\grad p. \label{eqn:mom}
\end{align}
To perform the linearization, we note that the static fluid at the ambient pressure and density satisfies the governing equations (\ref{eqn:state}-\ref{eqn:mom}). This trivial solution, called the base state or the unperturbed state, is given by
\begin{align}
 p = p_0, \qquad \rho = \rho_0, \qquad \bu = \boldsymbol{0}.
\label{eqn:base}
\end{align}
We now perturb this state infinitesimally as
\begin{align}
 p = p_0 + \epsilon p', \qquad \rho = \rho_0 + \epsilon \rho', \qquad \bu = \boldsymbol{0} + \epsilon \bu',
 \label{eqn:perturb}
\end{align}
where we consider the limit of the scalar parameter $\epsilon \to 0$ to impose the infinitesimal size of the perturbation.
Substituting these perturbed quantities in (\ref{eqn:state}-\ref{eqn:mom}) yields
\begin{align}
\epsilon p' = p_0 \left[\left(1 + \epsilon \dfrac{\rho'}{\rho_0} \right)^\gamma - 1\right], \label{eqn:state1} \\
\pd{\rho_0}{t} + \epsilon \pd{\rho'}{t} + \epsilon \grad\cdot((\rho_0+\epsilon\rho') \bu) &= 0, \label{eqn:mass1} \\
(\rho_0 + \epsilon \rho') \left( \epsilon \pd{\bu'}{t} +\epsilon^2 \bu' \cdot\grad\bu' \right) &= -\grad p_0. - \epsilon \grad p' \label{eqn:amom1}
\end{align}
Note that $\pd{\rho_0}{t}$ and $\grad p_0$ vanish.
We now expand these equations in a series in $\epsilon$.
\begin{align}
\epsilon p' = p_0 \left[\epsilon \gamma \dfrac{\rho'}{\rho_0} + \epsilon^2 \dfrac{\gamma (\gamma-1)}{2} \left(\dfrac{\rho'}{\rho_0}\right)^2 +  \dots \right], \label{eqn:state2} \\
\epsilon \left( \pd{\rho]}{t} + \rho_0 \grad\cdot\bu \right) + \epsilon^2 \grad\cdot(\rho' \bu') = 0, \label{eqn:mass2} \\
\epsilon \rho_0 \pd{\bu}{t} + \epsilon^2 \left( \rho' \pd{\bu'}{t} + \rho_0 \bu' \cdot\grad\bu' \right) + \epsilon^2 \rho' \bu' \grad\bu' = - \epsilon \grad p'. \label{eqn:amom2}
\end{align}
Note that in the limit $\epsilon \to 0$, each of the terms in these equations vanish, thus ascertaining the validity of the base state as a valid solution of the governing equations. To derive the equations governing the perturbations, it is necessary to divide each term by a factor of $\epsilon$, and then take the limit as $\epsilon\to 0$. This yields
\begin{align}
 p' = \dfrac{\gamma p_0}{\rho_0} \rho', \label{eqn:statelin} \\
 \pd{\rho'}{t} + \rho_0 \grad\cdot\bu' = 0, \label{eqn:masslin} \\
 \rho_0 \pd{\bu'}{t} = - \grad p'. \label{eqn:momlin}
\end{align}
Eliminating $\rho'$ and $\bu'$ to derive a single equation for pressure as
\begin{align}
\pdd{p'}{t^2} = c^2 \nabla^2 p' \qquad \text{where} \qquad c^2 = \dfrac{\gamma p_0}{\rho_0}.
\end{align}
This is the wave equation for the perturbation to the pressure and $c$ is the speed of the wave, in this case the sound wave. In other words, small perturbations in pressure propagate as sound waves at a speed given by $c$. 

\section{Incompressible approximation}
The incompressibility condition neglects the variations of density of the fluid. We are now in a position to derive conditions based on dimensional analysis for the assuming the fluid to be incompressible. These conditions are of two kinds. The first ensures that the pressure variations in the fluid are not so large as to induce appreciable changes in the density. And the second ensures that the temporal dynamics associated with propagation of pressure impulses may be considered essentially instantaneous. In what follows, we will take a flow with characteristic length, time, and speed to be $L$, $T$ and $U$, respectively. We will take the ambient density and pressure to be $\rho_0$ and $p_0$, respectively.

\subsection{Variations in density due to pressure}
In an incompressible flow, the pressure field establishes itself to ascertain that the density of the fluid does not change due to the flow of material. Therefore, the magnitude of pressure variations is set by the strongest forces in the flow. For flows with large Reynolds numbers, this force is the fluid inertia, whereas at small Reynolds numbers, it is the fluid viscosity. Therefore, we need two different cases.
\subsubsection{Inertially-dominated flows}
When the fluid inertia is the dominant force in the fluid, the variations in pressure generally (but not always\footnote{For example, the pressure is uniform in Stokes first problem, despite inertia being a dominant force and the scale of fluid velocity variations being $U\neq 0$.}) scale as $\rho U^2$. The scaling estimate for the corresponding change in density is 
\begin{align}
 \rho' = \dfrac{\rho_0 U^2}{\left(\pd{p}{\rho}\right)}.
\end{align}
Noting that $\pd{p}{\rho}$ scales with $c^2$, where $c$ is the speed of sound in the medium, we arrive at
\begin{align}
 \dfrac{\rho'}{\rho_0} = \dfrac{U^2}{c^2} = \Ma^2, \qquad \text{where} \qquad \Ma = \dfrac{U}{c}.
\end{align}
Here $\Ma$ is the Mach number. The flow may be approximated to be incompressible if $\Ma \ll 1$. For aerospace applications, when $\Ma \lesssim 0.3$, the flow is found to be approximated well by the incompressible assumption. For other applications, the mileage of this condition may vary.

\subsubsection{Viscously-dominated flows}
The scale for pressure variations in a viscously-dominated flow is $\mu U/L$. The corresponding variations in density are
\begin{align}
 \dfrac{\rho'}{\rho_0} = \dfrac{\mu U/L}{\rho_0\left(\pd{p}{\rho}\right)} = \dfrac{\Ma^2}{\Rey}.
\end{align}
Thus, when $\Rey$ is small, the condition for violating incompressibility is that $\Ma^2$ be much larger than $\Rey$. For practical materials, this condition is difficult to violate. 

\subsection{The finite propagation-speed of sound}
The second criteria for incompressibility is that sound propagation be considered instantaneous. Given the finite speed of propagation of sound, pressure pulses travel a distance $cT$ in time $T$. If the transient dynamics on time-scale $T$ are of interest to the analysis (e.g., the flow may be driven on a rapid time scale), and the system size of interest is much larger than $cT$, then the propagation of sound waves may not be ignored. This condition may be stated as
\begin{align}
 \dfrac{L}{cT} \ll 1,
\end{align}
for incompressibility to be a valid approximation to the resulting flow.
