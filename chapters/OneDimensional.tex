\chapter{One-dimensional flows}
In this chapter, we will treat exact solutions of Navier-Stokes equations that result from the assumption of ``one-dimensionality''. Such flows do occur in nature and are highly relevant for many common applications.

\section{Steady flows}
Several key features are common amongst the one-dimenional solutions of Navier-Stokes equations, which are summarized here.

\subsection{One-dimensionality}
The assumption of one-dimensionality underlies all the flows presented in this chapter. One-dimensionality means that in either Cartesian or cylindrical-polar coordinates, the velocity has only one non-zero component of velocity, which varies in a direction perpendicular to the flow. Here are examples, which are illistrated in Figure~\ref{fig:OnedSchematic}:

\begin{figure}[h]
\centerline{\includegraphics{OnedSchematic}}
\caption{Schematic of three cases of one-dimensional flows.}
\label{fig:OnedSchematic}
\end{figure}

\begin{enumerate}
 \item {\bf Plane Couette-Poiseuille flow:} This is a flow of a fluid sandwiched between two infinite planar walls, shown in Figure~\ref{fig:OnedSchematic}(a). In this case the flow is along a single direction $x$ parallel to the wall and the flow velocity varies in the direction $y$ perpendicular to the walls.
 \item {\bf Cylindrical Couette-Poiseuille flow}, some cases of which are also known as {\bf Hagen-Poiseuille flow:} This is the flow in the axial (i.e., $z$) direction through the gap between two infinitely-long coaxial cylinders. The flow varies only with $r$, the radial distance from the axis. 
 \item {\bf Circular Couette flow:} This is the flow of a fluid in the azimuthal (i.e., $\e_\theta$) direction between two coaxial cylinders. The flow velocity varies only with the radial distance from the axis, i.e.  $r$.
\end{enumerate}

In practice, the walls on plane Couette-Poiseuille flow and the cylinders in the cylindrical Couette Poiseuille flow are not infinitely long. This is only meant as an approximation for the case where they are much longer than the width of the gap through which the flow occurs.

In all these cases, the component of the acceleration of the fluid in the direction of the flow is zero. In fact, the acceleration is zero for the case of plane and cylindrical Couette-Poiseuille flow. For circular Couette flow the acceleration is centripetal. Therefore, when solving for the flow using the Navier-Stokes equations, the inertial term drops out of the momentum balance in the direction of the flow.

\subsection{The driving force}
The flow could be drive by one of the following three agencies:
\begin{enumerate}
 \item {\bf Pressure gradient:} A gradient of pressure is said to have been set and driving the flow when an external agency, e.g., a pump, increases the pressure at the inlet of the pipe or channel, or decreases it far downstream. In this case, the flow is driven by the pressure decreasing along the length of the channel in the direction of the flow. The resulting flow is called a Poiseuille flow, after the French physicist and physiologist Jean Léonard Marie Poiseuille, who used this type of analysis to study blood flow through capillaries. The general type of question that arises for these types of flow are about the pressure gradient needed to drive a certain volumetric flow rate through the domain.
 \item {\bf Gravity:} Similar to pressure gradient, the flow could be driven by an external volumetric force, such as gravity. 
 \item {\bf Motion of the walls:} An external agency could push on the walls to move along the direction of the flow, thereby dragging the fluid next to them. The general question that comes up in such cases is the shear stress needed to move the walls at prescribed speeds. This type of flow is called Couette flow, after the French physicist Maurice Marie Alfred Couette.
\end{enumerate}
Note that in circular Couette flow either a pressure gradient or a constant force of gravity cannot drive the flow. This is so because of periodicity in the direction of the flow. Pressure cannot continually decrease along the flow because after an angle of $2\pi$ we return back to the original point, where we expect to find the same pressure as we started. Therefore, circular Couette flow may only be driven by motion of the walls.

All three methods of driving the flow can be used in instruments that measure the dynamic viscosity of the fluid.

\section{Select solutions of Navier-Stokes}
Here we present some examples of solutions of Navier-Stokes equations for steady one-dimensional flows.
\subsection{Plane Poiseuille flow}
This case corresponds to the pressure-driven flow in a channel. The walls of the channel are stationary in the lab frame in this case.

Given the flow profile $\bu = u(y) \e_x$, the equation of continuity is trivially satisfied and the two-dimensional Navier-Stokes equations in Cartesian coordinates simplify to
\begin{subequations}
\begin{align}
 \pd{p}{x} &= \mu \pdd{u}{y}, \label{eqn:ppfx}\\
 \pd{p}{y} &= 0. \label{eqn:ppfy}
\end{align}
\end{subequations}
According to \eqref{eqn:ppfy}, the pressure does not vary with $y$, and hence we can assert that $p(x)$ alone. Then in \eqref{eqn:ppfx}, the left-hand side depends on $x$ alone, whereas the right-hand side on $y$ alone, implying that both sides must be a constant. Physically, because the flow does not change with $x$, we do not expect the pressure gradient to vary with $x$ either. Using the constancy of $\partial p/\partial x$, \eqref{eqn:ppfy} may be readily integrated to yield
\begin{align}
 u(y) = \dfrac{1}{2\mu} \pd{p}{x} y^2 + Ay + B,
\end{align}
where $A$ and $B$ are constants of integration. They are determined by the no-slip boundary conditions on the two walls
\begin{align}
 u\left( y = \pm \dfrac{H}{2} \right) = 0,
\end{align}
which yields 
\begin{align}
 u(y) = - \dfrac{1}{2\mu} \pd{p}{x} \left( \dfrac{H^2}{4} - y^2 \right).
\end{align}
The flow rate per unit width $Q/w$ is
\begin{align}
 \dfrac{Q}{w} = -\int_{-H/2}^{H/2} u(y)~dy = -\dfrac{1}{2\mu} \pd{p}{x} \left( \dfrac{H^2y}{4} - \dfrac{y^3}{3} \right)_{-H/2}^{H/2} = -\dfrac{H^3}{12\mu} \pd{p}{x}. 
\end{align}
This is the relation between the pressure-gradient and the flow rate that is practically sought. Experimental measurements of $H$, $\partial p/\partial x$ and $Q/w$ may be used to determine the dynamic viscosity $\mu$.

Also worth noting is the result that the average fluid speed $Q/(wH)$ is 2/3 of the maximum fluid speed $u(y=0)$.

\subsection{Hagen-Poiseuille}
This is the flow driven by a pressure gradient inside a pipe. In this case, the inside cylinder shown in Figure~\ref{fig:OnedSchematic}(b) is absent the fluid fills all the space inside the outer cylinder. Also, the outer cylinder is stationary.

Continuity is trivially satisfied by the velocity profile given by $\bu = w(r) \e_z$. The radial and axial components of the Navier-Stokes equations, \eqref{eqn:nscylr} and \eqref{eqn:nscylz}, respectively, simplify to
\begin{align}
 \pd{p}{r} &= 0, \label{eqn:hpr} \\
 \pd{p}{z} &= \dfrac{\mu}{r} \pd{}{r} \left( r \pd{w}{r} \right). \label{eqn:hpz}
\end{align}
Similar to the case of plane Poiseuille flow, here the pressure depends on $z$ alone and must decrease at a rate independent of $z$, i.e. $\partial p/\partial z$ is a constant. The $z$-momentum equation may then be integrated ot yield
\begin{align}
 r \pd{w}{r} = \dfrac{r^2}{2\mu} \pd{p}{z} + A, \qquad w(r) =  \dfrac{r^2}{4\mu} \pd{p}{z} + A \ln r + B, \label{eqn:hpzint}
\end{align}
where $A$ and $B$ are constants of integration.

At this stage, it appears that we only have one boundary condition $w(r=R) = 0$ corresponding to no-slip on the outer wall at $r=R$. It is commonly (and in my opinion incorrectly) stated that a second condition is that the velocity cannot blow up on the axis $r=0$ and hence $A=0$. Then, one uses no-slip on $r=R$ to obtain
\begin{align}
 w(r) =  -\dfrac{1}{4\mu} \pd{p}{z} (R^2-r^2).
\end{align}
A much more convincing argument for setting $A=0$ is as follows. Consider the surface of an imaginary cylinder at radius $r$. The axial force exerted by the fluid inside of the cylinder on the fluid outside is
\begin{align}
 \tilde{F}_z = 2 \pi r \sigma_{rz} = 2 \pi r \mu \pd{w}{r} = 2 \pi \left( \dfrac{r^2}{2\mu} \pd{p}{z} + A \right),
\end{align}
where we have used \eqref{eqn:hpzint} in the final step. Now consider the limit of $r$ approaching zero, i.e. the imaginary cylinder shrinking towards the axis. In the limit $\tilde{F}_z$ approaches $2 \pi A$. Based on this, it seems that $A$ is related to the influence of an imaginary agency on the axis of the cylinder to exert a shear force on the fluid. Since we do not have any such agency present in the problem under consideration, $\tilde{F}_z$ must vanish, and so must $A$.

The relation between pressure gradient and the volumetric flow rate through the pipe $Q$ may also be found easily as
\begin{align}
 Q = \int_0^R 2 \pi r w(r)~dr = -\dfrac{2 \pi }{4\mu} \pd{p}{z} \int_0^R r (R^2-r^2)~dr = -\dfrac{2 \pi }{4\mu} \pd{p}{z} \left(\dfrac{R^2r^2}{2} - \dfrac{r^4}{4} \right)_0^R = - \dfrac{\pi R^4}{8\mu} \pd{p}{z}.
\end{align}

Also worth noting is the result that the average fluid speed $Q/(\pi R^2)$ is 1/2 of the maximum fluid speed $w(r=0)$.

\section{Unsteady flows}
If condition of steadiness is relaxed, then the governing equations simplify to a partial differential equation for the flow profile that depends only on one spatial coordinate and time. Here we present one such example. The same three geometrical categories and the three kinds of driving forces can be used to classify this type of flow. We will not repeat them here.

Because of the partial nature of the differential equation, the solution procedure depends on the particular problem and can be a little involved. It is illustrated for the case of Stokes first problem below.

\subsection{Stokes first problem}
An infinite flat plate borders a semi-infinite layer of fluid of density $\rho$ and viscosity $\mu$, see Figure~\ref{fig:StokesFirst}. The fluid and the plate are initially stationary. At $t=0$, the plate impulsively starts to translate parallel to itself with speed $U$. This drags the fluid adjacent to the plate.

\begin{figure}
\centerline{\includegraphics{StokesFirst}}
\caption{Schematic setup of the Stokes first problem.}
\label{fig:StokesFirst}
\end{figure}

In this case, the fluid velocity may be assumed to be one-dimensional as $\bu = u(y,t) \e_x$. With this assumption, the velocity profile satisfies continuity exactly. The governing Navier-Stokes equations simplify to
\begin{align}
 \rho \pd{u}{t} &= - \pd{p}{x} + \mu \pdd{u}{y}, \label{eqn:stokesxmom} \\
 0 &= -\pd{p}{y}. \label{eqn:stokesymom}
\end{align} 
The $y$-momentum balance \eqref{eqn:stokesymom} implies $p$ does not vary with $y$. Combining this with the condition far away from the plate, where the fluid is stationary and therefore the pressure is a constant, implies that the pressure is constant everywhere. This implies that the pressure gradient in \eqref{eqn:stokesxmom} vanishes.

The governing equations and the boundary conditions now become
\begin{subequations}
\begin{align}
 \pd{u}{t} = \nu \pdd{u}{y}, \label{eqn:heat} \\
 u(y=0, t) = U, \label{eqn:wallbc} \\
 u(y\to\infty, t) = 0, \label{eqn:farfieldbc} \\
 u(y,t=0) = 0. \label{eqn:ic}
\end{align}
\label{eqn:pde}
\end{subequations}
This equation possesses a scaling symmetry. To see this transform
\begin{align}
 t = \alpha\tr{t}, \quad y = \beta\tr{y}, \quad u = \gamma \tr{u}.
\end{align}
The transformed equations are
\begin{subequations}
\begin{align}
 \pd{\tr{u}}{\tr{t}} = \nu \dfrac{\alpha}{\beta^2} \pdd{\tr{u}}{\tr{y}}, \label{eqn:heattr} \\
 \tr{u}(\tr{y}=0, \tr{t}) = \dfrac{U}{\gamma}, \label{eqn:wallbctr} \\
 \tr{u}(\tr{y}\to\infty, \tr{t}) = 0, \label{eqn:farfieldbctr} \\
 \tr{u}(\tr{y},\tr{t}=0) = 0. \label{eqn:ictr}
\end{align}
\label{eqn:pdetr}
\end{subequations}
Equations \eqref{eqn:pdetr} reduce to \eqref{eqn:pde} if $\gamma=1$ and $\alpha=\beta^2$, which implies that the solution must satisfy the transformation rule
\begin{align}
 u(y,t) = \tr{u}(\tr{y}, \tr{t}) = u\left( \dfrac{y}{\beta}, \dfrac{t}{\beta^2} \right),
\end{align}
where $\beta$ remains a free parameter.
Since we are free to choose any value for $\beta$, let us choose $\beta^2 = t$, so that the profile $u(y,t)$ at any time $t$ is related to the profile $u\left(\dfrac{y}{\sqrt{t}}, 1\right)$. Loosely speaking, the scale for $y$ increases as $\sqrt{t}$ with time.

An apparently casual, but in fact equally rigorous, way of arriving at the same result is to perform a rough scaling balance, where, as a matter of notation, we replace partial derivatives with ratios
\begin{align}
 \pd{u}{t} \sim \dfrac{u}{t}, \quad \pd{u}{y} \sim \dfrac{u}{y}, \quad \pdd{u}{y} \sim \dfrac{u}{y^2},
\end{align}
where the `$\sim$' symbols stands for `scales as'. Equation \eqref{eqn:pde} then becomes
\begin{align}
 \dfrac{u}{t} \sim \nu \dfrac{u}{y^2} \qquad \text{or} \qquad y \sim \sqrt{\nu t},
 \label{eqn:scaling}
\end{align}
which gives us the dependence of the scale of $y$ with $t$. Using \eqref{eqn:wallbc}, the scale for $u$ is $U$.

This sugests the functional form for the similarity variable
\begin{align}
 \xi = \dfrac{y}{\delta(t)} \quad \text{where} \quad \delta(t) = {2\sqrt{\nu t}},
\end{align}
and the factor of 2 in $\delta$ is for future algebraic convenience. Here $\delta$ represents the scale for $y$.
The following expressions are useful
\begin{align}
 \dfrac{d\delta}{dt} = \dfrac{1}{2} \dfrac{\delta}{t}, \quad \pd{\xi}{t} = -\dfrac{1}{2} \dfrac{\xi}{t}, \quad \pd{\xi}{y} = \dfrac{1}{\delta}.
\end{align}
Using these scales, we present an ansatz for the form of the velocity profile $u(y,t) = U f(\xi)$, where $U$ imparts the dimensions to $u$ and the function $f(\xi)$ is dimensionless. This implies
\begin{align}
 \pd{u}{t} &= U f'(\xi) \pd{\xi}{t} = -\dfrac{U \xi}{2t} f'(\xi), \\
 \pdd{u}{y} &= \dfrac{U}{\delta^2} f''(\xi).
\end{align}
The partial differential equation \eqref{eqn:pde} becomes
\begin{align}
 -\dfrac{U \xi}{2t} f'(\xi) = \nu \dfrac{U}{\delta^2} f''(\xi) \quad \Longrightarrow \quad f''(\xi) + 2 \xi f'(\xi) = 0. 
 \label{eqn:ode}
\end{align}
In the first part of this equation, notice how the dimensional parts ($U/t$ and $\nu U/\delta^2$) of the terms are closely related to the terms ($u/t$ and $\nu u/y^2$) in the \eqref{eqn:scaling}, and they cancel each other because of our choice of $\delta$ determined by the scaling balance. Solving the ordinary differential equation in \eqref{eqn:ode} yields the general solution
\begin{align}
 f(\xi) = A + B \text{erf}(\xi),
\end{align}
where erf stands for the error function, and $A$ and $B$ are constants of integration. The constants are determined by the boundary conditions (\ref{eqn:wallbc}-\ref{eqn:ic}), which on $f$ become $f(0) = 1$ and $f(\infty) = 0$. The particular solution is then
\begin{align}
 f(\xi) = 1 - \text{erf}(\xi) = \text{erfc}(\xi),
\end{align}
erfc stands for the complimentary error function.

Interested students can try out the following problems of this kind:
\begin{enumerate}
 \item the flow in the annular gap between two co-axial cylinders (see Figure~\ref{fig:OnedSchematic}c) driven by the rotation of the outer or the inner cylinder,
 \item the flow driven simultaneously by pressure gradient, gravity and the motion of walls in the planar or cylindrical geometry (see Figure~\ref{fig:OnedSchematic}ab),
 \item the analogue of Stokes first problem, but where the wall velocity $U=\alpha t$, i.e. the wall accelerates with a constant acceleration $\alpha$,
 \item Stokes second problem, where the wall velocity oscillates sinusoidally for perpetuity.
\end{enumerate}
