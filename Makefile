SHELL = bash
HTMLDIR = html
TEXFILES=$(shell ls *.tex chapters/*.tex)
JSFILES=$(shell ls css/*.js)
CSSFILES=$(shell ls css/*.css)
JSSTR := $(foreach F, $(JSFILES), --javascript=$F) 
CSSSTR := $(foreach F, $(CSSFILES), --css=$F) 
MAINTEX=$(shell basename `grep -l documentclass *.tex` .tex)

all: tag

tag:    html

html: date pdf html/book.html
	echo "Inside Makefile make html"
	mkdir -p ${HTMLDIR}
	mv ${MAINTEX}.pdf ${HTMLDIR}
	cp index.html ${HTMLDIR}
	mv last_update.txt ${HTMLDIR}

date:   ${TEXFILES}
	TZ=Europe/London date > last_update.txt

pdf:	date
	latexmk -pdf ${MAINTEX}
	# pdflatex book.tex && pdflatex book.tex && pdflatex book.tex && pdflatex book.tex

html/book.html:	date
	latexml --dest=${MAINTEX}.xml ${MAINTEX}
	mkdir -p ${HTMLDIR}
	latexmlpost ${CSSSTR} ${JSSTR} --split --splitat=chapter --navigationtoc=context --dest=${HTMLDIR}/${MAINTEX}.html ${MAINTEX}.xml

test:
	echo ${HTMLDIR}

clean:
	rm *.aux *.log *.out *.toc *.xml

.PHONY: clean

# command to download latest artifact
# curl --location --output artifacts.zip https://gitlab.com/shreyasmandre.publishing/dimensional-analysis/-/jobs/artifacts/main/download?job=build
# 
