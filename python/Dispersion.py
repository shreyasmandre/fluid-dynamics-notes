import numpy as np
import matplotlib.pyplot as plt

from matplotlib import rcParams
rcParams['text.usetex'] = True
rcParams['text.latex.preamble'] = [
    r'\usepackage{amsmath}',
    r'\usepackage{amssymb}']
rcParams['font.family'] = 'serif'
# rcParams['font.serif'] = 'Computer Modern'


kmin = 0.8e-1
kmax = 5
N = 100

k = np.linspace(kmin, kmax, N)
omegag = 1/k**0.5
omegac = k**0.5
omega = (k+1/k)**0.5

plt.clf()
fig = plt.figure(1, figsize=(3,2))
ax = fig.add_subplot(111)
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)

ax.plot(k, omega, 'k-', linewidth=3)
ax.plot(k, omegag, '-.', color='blue')
ax.plot(k, omegac, '--', color='red')

ax.set_xlabel('$k$')
ax.set_ylabel('$c(k)$')

ax.set_xticks([1])
ax.set_yticks([2**0.5])
ax.set_xticklabels(['$k_{*}$'])
ax.set_yticklabels(['$c_{*}$'])
# ax.text(3, 1, '$\left[ \left(\dfrac{\\rho_w-\\rho_a}{ \\rho_w + \rho_a}\\right) \dfrac{g}{|k|} \\right]^{1/2}$')
ax.text(3, 0.65, '$c_\\text{grav}(k) = \left[ \left(\dfrac{\\rho_w-\\rho_a}{ \\rho_w + \\rho_a}\\right) \dfrac{g}{|k|} \\right]^{1/2}$', color='blue')
ax.text(3, 1.45, '$c_\\text{cap}(k) = \left[ \left(\dfrac{ \sigma |k| }{ \\rho_w + \\rho_a}\\right)\\right]^{1/2}$', color='red')

ax.plot([1, 1,], [0, 2**0.5], 'k:')
ax.plot([0, 1,], [2**0.5, 2**0.5], 'k:')

ax.set_xlim([0, kmax])
ax.set_ylim([0, 3.5])
plt.show(block=False)

fig.savefig('../Figures/Dispersion.pdf')
fig.savefig('../Figures/Dispersion.svg')
