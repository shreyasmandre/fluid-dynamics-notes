import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import scipy.optimize as so

from matplotlib import rcParams
rcParams['text.usetex'] = True
rcParams['text.latex.preamble'] = [
    r'\usepackage{amsmath}',
    r'\usepackage{amssymb}']
rcParams['font.family'] = 'serif'
# rcParams['font.serif'] = 'Computer Modern'


Lx = 3
Ly = 3
Rmax = 10
Nx = 40
Ny = 40
Nr = 400
Nt = 400
x = np.linspace(-Lx, Lx, Nx)
y = np.linspace(-Ly, Ly, Ny)
r = np.linspace(1e-2, Rmax, Nr)
t = np.linspace(0, 2*np.pi-1e-3, Nt)

X, Y = np.meshgrid(x,y)
R, T = np.meshgrid(r,t)
Z = X+1j*Y
Zr = R*np.exp(1j*T)

fig = plt.figure(1, figsize=(6.5,6))
fig.clf()
ax1 = fig.add_subplot(311)
ax2 = fig.add_subplot(312, sharex=ax1)
ax3 = fig.add_subplot(313, sharex=ax1)
ax1.axis('scaled')
ax2.axis('scaled')
ax3.axis('scaled')

Q = 0.5
W = Zr + 1/Zr
X = Zr.real
Y = Zr.imag

rc = 1 + 0*t

philevels = np.linspace(-8, 10, 31)
psilevels = np.linspace(-3, 3, 31)
ax1.contour(X, Y, W.real, levels=philevels, colors='r', linewidths=0.5)
ax1.contour(X, Y, W.imag, levels=psilevels, colors='b', linewidths=0.5)
# ax1.contour(X, Y, W.imag, levels=[0], colors='b', linewidths=0.5)

ax2.contour(X, Y, W.real, levels=philevels, colors='r', linewidths=0.5)
ax2.contour(X, Y, W.imag, levels=psilevels, colors='b', linewidths=0.5)
# ax2.contour(X, Y, W.imag, levels=[0], colors='k', linewidths=2)
ax2.fill(rc*np.cos(t), rc*np.sin(t))
ax2.plot(rc*np.cos(t), rc*np.sin(t), 'k-')

W = W + 1j*np.log(-Zr)
philevels = np.linspace(-3*np.pi, 4*np.pi, 71)
psilevels = np.linspace(-np.pi, 2*np.pi, 31)
ax3.contour(X, Y, W.real, levels=philevels, colors='r', linewidths=0.5)
ax3.contour(X, Y, W.imag, levels=psilevels, colors='b', linewidths=0.5)
ax3.fill(rc*np.cos(t), rc*np.sin(t))
ax3.plot(rc*np.cos(t), rc*np.sin(t), 'k-')

ax1.set_xlim([-8,8])
ax1.set_ylim([-2.5,2.5])
# ax2.set_xlim([-6,6])
ax2.set_ylim([-2.5,2.5])
ax3.set_ylim([-2.5,2.5])
plt.setp(ax1.get_xticklabels(), visible=False)
plt.setp(ax2.get_xticklabels(), visible=False)

ax3.set_xlabel('$x$')
ax3.set_ylabel('$y$')

ax1.text(-7.7, -2.3, '(a)')
ax2.text(-7.7, -2.3, '(b)')
ax3.text(-7.7, -2.3, '(c)')

plt.show(block=False)
plt.savefig('../Circle.pdf')
plt.savefig('../Circle.svg')

