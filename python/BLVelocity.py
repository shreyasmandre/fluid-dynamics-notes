import numpy as np
import matplotlib.pyplot as plt

from matplotlib import rcParams
rcParams['text.usetex'] = True
rcParams['text.latex.preamble'] = [
    r'\usepackage{amsmath}',
    r'\usepackage{amssymb}']
rcParams['font.family'] = 'serif'
# rcParams['font.serif'] = 'Computer Modern'

# Visualize COMSOL results for flow in the boundary layer
d = np.loadtxt('BLVelocity.txt', comments='%')
x = d[:7,0]
y = d[::7,1]
y = y - y[0]
y = y*1e3
d = d[:,2:]
mu = np.array([1e-5, 1e-5, 1e-5, 1e-5, 3e-5, 3e-5, 3e-5, 3e-5, 5e-5, 5e-5, 5e-5, 5e-5])
U  = np.array([1, 1.3, 1.6, 1.9, 1, 1.3, 1.6, 1.9, 1, 1.3, 1.6, 1.9, 1, 1.3, 1.6, 1.9])

fig1 = plt.figure(1, figsize=(12,6))
fig1.clf()
axes = []
for ii in range(4):
    for jj in range(3):
        idx = 3*ii+jj
        ax1 = fig1.add_subplot(3,4,idx+1)
        axes.append(ax1)
        for kk in range(7):
            ax1.plot(y, d[kk::7,idx], '-', label='$x={0:3.1f}$ cm'.format((x[kk]*1e2)))

ax1 = axes[8]
ax1.set_xlabel('$y$ (mm)')
ax1.set_ylabel('$u$ (m/s)')

labels = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)', '(g)', '(h)', '(i)', '(j)', '(k)', '(l)']
for label, ax1 in zip(labels, axes):
    ax1.set_xlim(0, 5)
    ax1.set_ylim(0, 2.1)
    ax1.text(4.5, 0.1, label)

axes[1].set_yticklabels([])
axes[2].set_yticklabels([])
axes[3].set_yticklabels([])

axes[5].set_yticklabels([])
axes[6].set_yticklabels([])
axes[7].set_yticklabels([])

axes[9].set_yticklabels([])
axes[10].set_yticklabels([])
axes[11].set_yticklabels([])

axes[0].set_xticklabels([])
axes[1].set_xticklabels([])
axes[2].set_xticklabels([])
axes[3].set_xticklabels([])
axes[4].set_xticklabels([])
axes[5].set_xticklabels([])
axes[6].set_xticklabels([])
axes[7].set_xticklabels([])

axes[0].set_title('$U=1.0$ m/s')
axes[1].set_title('$U=1.3$ m/s')
axes[2].set_title('$U=1.6$ m/s')
axes[3].set_title('$U=1.9$ m/s')

axes[0].text(-1.3, 1, '$\mu = 1 \\times 10^{-5}$ Pa s', horizontalalignment='center', verticalalignment='center', rotation=90)
axes[4].text(-1.3, 1, '$\mu = 3 \\times 10^{-5}$ Pa s', horizontalalignment='center', verticalalignment='center', rotation=90)
axes[8].text(-1.3, 1, '$\mu = 5 \\times 10^{-5}$ Pa s', horizontalalignment='center', verticalalignment='center', rotation=90)

axes[0].legend(loc='upper left', ncol=3, fontsize=8, handlelength=1, handletextpad=0.5, columnspacing=1, frameon=False)

fig1.tight_layout()
fig1.subplots_adjust(hspace=0.1, wspace=0.05)
plt.show(block=False)
fig1.savefig('../Figures/BLVelocity_Panels.pdf')
fig1.savefig('../Figures/BLVelocity_Panels.svg')

# 
# fig2 = plt.figure(2)
# fig2.clf()
# ax2 = fig2.add_subplot(111)
# for ii in [2,3,4,5]:
#     for jj in range(7):
#         ax2.plot(y/x[jj]**0.5, d[jj::7,ii], '-')
# 
# ax2.set_xlabel('$y/x^{1/2}$')
# ax2.set_ylabel('$u$')
# 
# plt.show(block=False)
# plt.waitforbuttonpress()

# Check for boundary layer self-similar scaling
fig3 = plt.figure(3, figsize=(5,3))
fig3.clf()
ax3 = fig3.add_subplot(111)
y = y*1e-3
for ii in range(12):
    for jj in range(7):
        delta = np.sqrt(2*mu[ii]*x[jj]/U[ii])
        ax3.plot(y/delta, d[jj::7,ii]/U[ii], '-')

ax3.set_xlabel('$y/\delta(x)$')
ax3.set_ylabel('$u/U$')

# Solve ODE for the self-similar profile and plot on top of collapsed data
def fun(x, f):
    return np.array([f[1], f[2], -f[0]*f[2]])

xi = 0
dxi = 1e-3
N = 10001
F = np.zeros((N,3))
Xi = np.zeros((N,))
fpp = 0.4695999884592487
f = np.array([0, 0, fpp])

for ii in range(N):
    F[ii,:] = f
    Xi[ii] = xi
    k1 = dxi*fun(xi, f)
    k2 = dxi*fun(xi+dxi/2, f+k1/2)
    k3 = dxi*fun(xi+dxi/2, f+k2/2)
    k4 = dxi*fun(xi+dxi, f+k3)
    f = f + (k1+2*k2+2*k3+k4)/6
    xi = xi + dxi

F[N-1,:] = f
Xi[N-1] = xi
print(F[-1,:])

ax3.plot(Xi, F[:,1], 'k--', linewidth=4)
ax3.set_xlim([0, 10])
fig3.tight_layout()
plt.show(block=False)
fig3.savefig('../Figures/BLVelocity_Collapse.pdf')
fig3.savefig('../Figures/BLVelocity_Collapse.svg')

