% Model:              FlatPlateBoundaryLayer.mph
% Version:            COMSOL 5.5.0.306
% Date:               Mar 6 2020, 13:30
% Table:              Skin Friction - Skin Friction drag
% Uinf (m/s)             mu=1E-5, -spf.T_stressx (N/m)mu=3E-5, -spf.T_stressx (N/m)mu=5E-5, -spf.T_stressx (N/m)
1                        4.1312940539225696E-4    7.856663624596253E-4     0.0010624853048659655
1.3                      5.991384056252915E-4     0.0011380978036430436    0.0015372818273456602
1.6                      8.040449946771763E-4     0.0015265284096693003    0.0020602333443100144
1.9                      0.001025317627693273     0.0019468629009746499    0.002625924960368106
