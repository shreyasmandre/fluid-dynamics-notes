import matplotlib.pyplot as plt
import numpy as np
from matplotlib import rcParams
rcParams['text.usetex'] = True
rcParams['text.latex.preamble'] = r'\usepackage{amsmath} \usepackage{amssymb}'
rcParams['font.family'] = 'serif'
# rcParams['font.serif'] = 'Computer Modern'

Movie = False
Static = True
# Nxc = 151
# Nyc = 101
# Lxc1 = -2
# Lxc2 = 2
# Lyc1 = -2
# Lyc2 = 3
# xc = np.linspace(Lxc1, Lxc2, Nxc)
# yc = np.linspace(Lyc1, Lyc2, Nyc)
# Xc,Yc = np.meshgrid(xc,yc)

Nr = 151
Ntheta = 100
rmax = 6
r = np.linspace(1e-3,rmax, Nr)
theta = np.linspace(-np.pi+1e-6, np.pi-1e-6, Ntheta)
R,Theta = np.meshgrid(r,theta)
Xc = R*np.cos(Theta)
Yc = R*np.sin(Theta)

Nx = 81
Ny = 51
Lx1 = -1
Lx2 = 0
Ly1 = 1
Ly2 = 2
xp = np.linspace(Lx1, Lx2, Nx)
yp = np.linspace(Ly1, Ly2, Ny)
Xp,Yp = np.meshgrid(xp,yp)
Q = 8
Gamma = 16
def psi(x,y,t):
  r = (x)**2 + (y)**2
  return (Q/2/np.pi)*np.arctan2(y,x) + (Gamma/4/np.pi)*np.log(r)
def vel(x,y,t):
  r = (x)**2 + (y)**2
  u = (Q/2/np.pi)*(x)/r + (Gamma/2/np.pi)*y/r
  v = (Q/2/np.pi)*(y)/r - (Gamma/2/np.pi)*x/r
  return u,v

Psic = psi(Xc,Yc,0)
Uc,Vc = vel(Xc,Yc,0)

xl = Xp.ravel().copy()
yl = Yp.ravel().copy()

Nt = 1001
dt = 2/(Nt-1)
t = 0
T = np.zeros((Nt,))
Xl = np.zeros((Nx*Ny, Nt))
Yl = np.zeros((Nx*Ny, Nt))
Xl[:,0] = xl
Yl[:,0] = yl

for ii in range(Nt-1):
  u1,v1 = vel(xl, yl, t)
  xtmp = xl + u1*dt/2
  ytmp = yl + v1*dt/2
  u2,v2 = vel(xtmp, ytmp, t+dt/2)
  xtmp = xl + u2*dt/2
  ytmp = yl + v2*dt/2
  u3,v3 = vel(xtmp, ytmp, t+dt/2)
  xtmp = xl + u3*dt
  ytmp = yl + v3*dt
  u4,v4 = vel(xtmp, ytmp, t+dt)
  xl += dt*(u1+2*u2+2*u3+u4)/6
  yl += dt*(v1+2*v2+2*v3+v4)/6
  Xl[:,ii+1] = xl
  Yl[:,ii+1] = yl
  t += dt

def plotgrid(idx, ax, alpha=1):
    c = ['b', 'r', 'g', 'c', 'm', 'y']
    nr = 4
    Xnew = Xl[:,idx].reshape(Xp.shape)
    Ynew = Yl[:,idx].reshape(Yp.shape)
    for ii in range(nr):
        for jj in range(nr):
            i1 = int(ii*Nx/nr)
            i2 = int((ii+1)*Nx/nr)
            j1 = int(jj*Ny/nr)
            j2 = int((jj+1)*Ny/nr)
            ax.scatter(Xnew[j1:j2,i1:i2], Ynew[j1:j2,i1:i2], c=c[(jj+nr*ii)%len(c)], s=0.2, alpha=alpha)

if Movie:
    fig = plt.figure(1)
    fig.clf()
    ax = fig.add_subplot(111)

    for ii in range(0,Nt,20):
        ax.cla()
        plt.contour(Xc,Yc,Psic - Psic.min(), 60, colors='k', linewidths=0.5)
        plotgrid(0, ax, alpha=0.1)
        plotgrid(ii, ax)
        ax.set_aspect('equal')
        ax.set_xlim([-4,4])
        ax.set_ylim([-4,4])
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_title('t={0:1.3f}'.format(ii*dt))
        plt.pause(1e-3)
        plt.show(block=False)

if Static:
    fig2 = plt.figure(2, figsize=(3,3))
    fig2.clf()
    ax2 = fig2.add_subplot(111)
    ax2.contour(Xc,Yc,Psic - Psic.min(), 60, colors='k', linewidths=0.5)
    plotgrid(0, ax2, alpha=0.1)
    plotgrid(-1, ax2)
    Xnew = Xl[:,-1].reshape(Xp.shape)
    Ynew = Yl[:,-1].reshape(Yp.shape)
    ax2.arrow(Xp[0,-1], Yp[0,-1], Xnew[0,-1]-Xp[0,-1], Ynew[0,-1]-Yp[0,-1], width=0.075, length_includes_head=True, lw=0)
    ax2.arrow(Xp[-1,0], Yp[-1,0], Xnew[-1,0]-Xp[-1,0], Ynew[-1,0]-Yp[-1,0], width=0.075, length_includes_head=True, lw=0)
    ax2.arrow(Xp[-1,-1], Yp[-1,-1], Xnew[-1,-1]-Xp[-1,-1], Ynew[-1,-1]-Yp[-1,-1], width=0.075, length_includes_head=True, lw=0)
    ax2.arrow(Xp[0,0], Yp[0,0], Xnew[0,0]-Xp[0,0], Ynew[0,0]-Yp[0,0], width=0.075, length_includes_head=True, lw=0)

    ax2.set_aspect('equal')
    ax2.text(-2.2, 1.75, '$M_{t=0}$')
    ax2.text(2.5, 2, '$M_{t=2}$')
    ax2.set_xlim([-4,4])
    ax2.set_ylim([-4,4])
    ax2.set_xlabel('$x$')
    ax2.set_ylabel('$y$')
    fig2.tight_layout()
    plt.show(block=False)
    fig2.savefig('../Figures/LagrangianDeformation.pdf')
    fig2.savefig('../Figures/LagrangianDeformation.svg')

