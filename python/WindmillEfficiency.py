import numpy as np
import matplotlib.pyplot as plt

from matplotlib import rcParams
rcParams['text.usetex'] = True
rcParams['text.latex.preamble'] = [
    r'\usepackage{amsmath}',
    r'\usepackage{amssymb}']
rcParams['font.family'] = 'serif'
# rcParams['font.serif'] = 'Computer Modern'

a = np.linspace(0.5,1,21)
eta = 4*a**2*(1-a)
CD = 4*a*(1-a)

amax = 2.0/3
etamax = 16.0/27
CDmax = 8.0/9

fig = plt.figure(1, figsize=(4,2.5))
fig.clf()
ax = fig.add_subplot(111)
ax2 = ax.twinx()

ax.plot(a, eta, 'b-')
ax2.plot(a, CD, 'g-')
ax2.plot([amax,amax], [0,CDmax], 'k:')
ax.plot([0,amax], [etamax,etamax], 'k:')
ax2.plot([amax,1], [CDmax,CDmax], 'k:')
ax.plot(amax, etamax, 'bo')
ax2.plot(amax, CDmax, 'go')

ax.set_xlim([0.5, 1])
ax.set_ylim([0, 1.02])
ax2.set_ylim([0, 1.02])
ax.set_xticks([0.5, amax, 1])
ax2.set_xticks([0.5, amax, 1])
ax.set_xticklabels(['$1/2$', '$a_\\text{max}$', '$1$'])
ax.set_yticks([0, etamax, 1])
ax2.set_yticks([0, CDmax, 1])
ax.set_yticklabels(['$0$', '$16/27$', '$1$'])
ax2.set_yticklabels(['$0$', '$8/9$', '$1$'])
ax.set_xlabel('$a$')
ax.set_ylabel('$\eta$')
ax2.set_ylabel('$C_D$')

ax.text(amax-0.07,etamax-0.1, '$\eta_\\text{max}$')
ax.text(amax-0.1, CDmax-0.05, '$C_{D,\\text{max}}$')

fig.tight_layout()
plt.show(block=False)
fig.savefig('../Figures/WindmillEfficiency.pdf')
fig.savefig('../Figures/WindmillEfficiency.svg')

