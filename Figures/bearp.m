
figure(1), hold on
subplot('position',[.1 .1 .8 .4])
ylim([-.01 .26])
xlabel('$x/L$','interpreter','latex','fontsize',12)
ylabel('$(p-p_0)/(\mu UL/H_1^2)$','interpreter','latex','fontsize',12)
AL=.1:.1:1;
H1=1;
for j=1:length(AL);
    aL = AL(j);
    H2=aL+H1;
    p0=0;
    mu=1;
    x = 0:.01:1;
    h = (H1+aL*x);
    p=p0-(6*(1+aL)/(aL*(2+aL))).*(1./h.^2-(1./H1.^2))...
        +(6/aL).*(1./h-1./H1);

    c = [(length(AL)-j)/length(AL) 0 j/length(AL)];
    plot(x,p,'-','color',c), hold on
    
    hmax = 2*H1*H2/(H1+H2); xmax = (hmax-H1)/aL;
    pmax = p0-(6*H1.^3.*H2./aL./(H1+H2)).*(1./hmax.^2-(1./H1.^2))...
        +(6*H1.^2./aL).*(1./hmax-1./H1);
    plot(xmax,pmax,'p','color',c)
    
end
axes('Position',[.685 .355 .2 .12]), hold on
for j=1:length(AL);
    aL = AL(j);
    H2=aL+H1;
    c = [(length(AL)-j)/length(AL) 0 j/length(AL)];
    plot(x,H1+aL*x,'-','color',c)
    plot(0*[1 1],[H1 2.5],'-','color',c)
    plot([1 1],[H2 2.5],'-','color',c)
end
set(gca,'visible','on')
xlabel('$x/L$','interpreter','latex','fontsize',12)
ylabel('$h/H_1$','interpreter','latex','fontsize',12)
ylim([.9 2.5])