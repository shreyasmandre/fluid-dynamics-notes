clf
figure(1)
subplot('position',[.08 .11 .38 .36]), hold on
ylabel('$y/h$','interpreter','latex','fontsize',12), box on
xlabel('$U/U_{max}$','interpreter','latex','fontsize',12)
ylim([-1 1]), xlim([-.1 1.1])
y=-1:.01:1;
plot(1-y.^2,y,'k-','linewidth',.8)
n=.5;
plot(1-abs(y).^((n+1)/n),y,'b-','linewidth',.8)
n=2;
plot(1-abs(y).^((n+1)/n),y,'r-','linewidth',.8)
text(.69,-.25,'$n=2$','interpreter','latex','fontsize',10)
text(.84,-.45,'$1$','interpreter','latex','fontsize',10)
text(.92,-.52,'$\frac{1}{2}$','interpreter','latex','fontsize',10)
text(0,0,'$U_{max}=\frac{n}{n+1}\left(\frac{P_xh^{n+1}}{\mu}\right)^{1/n}$',...
    'interpreter','latex','fontsize',12)
title('Power-law','interpreter','latex','fontsize',12)

subplot('position',[.5 .11 .38 .36]), hold on
set(gca,'yticklabel',[]), box on
ylim([-1 1]), xlim([-.1 .8])
set(gca,'yaxislocation','right')
ylabel('$y/h$','interpreter','latex','fontsize',12)
xlabel('$U/\left(\frac{P_xh^2}{2K}\right)$','interpreter','latex','fontsize',12)
Y=.8;
y1=Y:.01:1; plot(1-y1.^2-2*Y*(1-y1),y1,'k-','linewidth',.8)
y2=-1:.01:-Y; plot(1-y2.^2-2*Y*(1+y2),y2,'k-','linewidth',.8)
plot([1 1]*(1-2*Y+Y^2),[-Y Y],'k-','linewidth',.8)
Y=.4;
y1=Y:.01:1; plot(1-y1.^2-2*Y*(1-y1),y1,'k-','linewidth',.8)
y2=-1:.01:-Y; plot(1-y2.^2-2*Y*(1+y2),y2,'k-','linewidth',.8)
plot([1 1]*(1-2*Y+Y^2),[-Y Y],'k-','linewidth',.8)
Y=.2;
y1=Y:.01:1; plot(1-y1.^2-2*Y*(1-y1),y1,'k-','linewidth',.8)
y2=-1:.01:-Y; plot(1-y2.^2-2*Y*(1+y2),y2,'k-','linewidth',.8)
plot([1 1]*(1-2*Y+Y^2),[-Y Y],'k-','linewidth',.8)
text(.05,0,'$\frac{\tau_Y}{P_x h}=0.8$','interpreter','latex','fontsize',12)
text(.38,0,'$0.4$','interpreter','latex','fontsize',12)
text(.65,0,'$0.2$','interpreter','latex','fontsize',12)
title('Bingham','interpreter','latex','fontsize',12)