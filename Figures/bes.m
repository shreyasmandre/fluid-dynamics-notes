x=0:0.001:1;
figure(1), hold on
plot(x,sqrt(x.*(1-x.^2).*besseli(1,x)./besseli(0,x)),'b-','linewidth',.7)
box on
xlabel('$kR_0$','interpreter','latex','fontsize',14)
ylabel('$\omega/\sqrt{(\gamma/\rho R_0^3)}$','interpreter','latex','fontsize',14)
[wm,ind]=max(sqrt(x.*(1-x.^2).*besseli(1,x)./besseli(0,x)));
plot([0 x(ind)],wm*[1 1],'k--')
plot(x(ind)*[1 1],[0 wm],'k--')
text(0.71,0.01,'0.697','interpreter','latex','fontsize',14)
text(0.015,0.33,'0.343','interpreter','latex','fontsize',14)